//
//  CommonFunction.swift
//  PizzaApp
//
//  Created by Jaydeep on 20/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import Foundation
import SwiftyJSON

func setupHomeArray() -> [JSON]
{
    var arrShop:[JSON] = []
    
    var dict = JSON()
    dict["name"].stringValue = getCommonString(key: "Cranbourne_west_key")
    dict["category_id"].stringValue = "1"
    arrShop.append(dict)
    
    dict = JSON()
    dict["name"].stringValue = getCommonString(key: "Cranbourne_north_key")
    dict["category_id"].stringValue = "2"
    arrShop.append(dict)
    
    dict = JSON()
    dict["name"].stringValue = getCommonString(key: "Balwyn_store_key")
    dict["category_id"].stringValue = "3"
    arrShop.append(dict)
    
    return arrShop
}

