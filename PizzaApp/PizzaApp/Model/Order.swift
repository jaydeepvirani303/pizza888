//
//  Order.swift
//  PizzaApp
//
//  Created by Khushbu on 18/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class Order: NSObject
{

    //MARK:- Future Order Date Time

    var strFutureOrderDate : String = ""
    var strFutureOrderTime : String = ""
    
    //MARK:- Apply Coupon Code
    
    var strCouponCode : String = ""
    
    //MARK:- Confirm Order
    
    var isSelectPaymentOption : Bool = false
    var strSelectPaymentOption : String = ""
    
    //MARK:- Select Store
    
    var strSelectStore : String = ""
    var strOrderNumber : String = ""
    var strComment : String = ""
    var strStorePhoneNumber : String = ""
    var strStoreAddress : String = ""

    var strSelectedOrderType = selectOrderType.pickup
    var strSelectedPickUpTime = selectedPickupTime.asap
    var strValidationMessage : String = ""
    var strSelectedStoreId : String = ""
    
    var strApplyCouponCode : String = ""
    
    var strProductType = String()
    var strServiceType = String()
    
    //MARK:- Validation Methiod

    func isCheckValidForFutureOrder() -> Bool
    {
        if (strFutureOrderDate.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_select_future_delivery_date_key")
            return false
        }
        else if (strFutureOrderTime.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_select_future_delivery_time_key")
            return false
        }
        return true
    }
    
    func isCheckValidForApplyCouponCode() -> Bool
    {
        if (strApplyCouponCode.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_coupon_code_key")
            return false
        }
        return true
    }
    
    func isCheckValidForConfirmOrder() -> Bool
    {
        if isSelectPaymentOption == false
        {
            self.strValidationMessage = getValidationString(key: "Please_select_payment_option_key")
            return false
        }
        return true
    }
    
    func isCheckValidForTracking() -> Bool
    {
        if (strSelectStore.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_select_store_key")
            return false
        }
        else if (strOrderNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_order_number_key")
            return false
        }
        return true
    }
    func isValidUserDetails(objUser:User) -> Bool
    {
        if (self.strSelectedOrderType == .pickup)
        {
            self.strValidationMessage = getValidationString(key: "Please_select_store_key")
            return false
        }
        else if (strOrderNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_order_number_key")
            return false
        }
        return true
    }
    func isCheckValidForOrder(objUser:User) -> Bool
    {
        
        var status = false
        
        if(self.strSelectedOrderType == .pickup)
        {
            status = objUser.isCheckValidForPickupStore()
            self.strValidationMessage = objUser.strValidationMessage
        }
        else
        {
            status = objUser.isCheckValidForDeliveryStore()
            self.strValidationMessage = self.strValidationMessage + "\n" + objUser.strValidationMessage

        }
        
        if(self.strSelectedPickUpTime == .future)
        {
            if(self.strFutureOrderDate.trimmed() == "")
            {
                status = false
                self.strValidationMessage = self.strValidationMessage + "\nPlease select Future order date"

            }
            else if(self.strFutureOrderTime.trimmed() == "")
            {
                status = false
                self.strValidationMessage = self.strValidationMessage + "\nPlease select Future order time"

            }
        }
        
        if(self.strSelectedStoreId.trimmed() == "")
        {
            status = false
            self.strValidationMessage = self.strValidationMessage + "\nPlease select store for your order"
        }
        
        return status
    }
}
