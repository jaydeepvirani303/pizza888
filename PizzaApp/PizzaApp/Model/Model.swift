
//
//  Model.swift
//  PizzaApp
//
//  Created by Jaydeep on 02/01/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import SwiftyJSON

class User: NSObject
{
    //MARK:- Store Detail Variable Declarattion
    
    var strName : String = ""
    var strPhoneNumber : String = ""      
    var strEmailAddress : String = ""
    var strUnitNumber : String = ""
    var strStreetNumber : String = ""
    var strStreetName : String = ""
    var strSuburbName : String = ""
    var strSuburbId : String = ""
    var strSuburbCost : String = ""
    var strDeliveryInsturction : String = ""
    var strReceiveMail : String = ""
    var strReceiveSMS : String = ""
    var strRememberPickupDetil : String = ""
    
    var strCardHolderName : String = ""
    var strCardNumber : String = ""
    var strExpiryYear : String = ""
    var strExpiryMonth : String = ""
    var strCVV : String = ""
    
    //MARK:- Return Validation message
    
    var strValidationMessage : String = ""
    
    //MARK:- Order Tracking
    
    var strChooseOrder : String = ""
    var strOrderNumber : String = ""
    
    
//    //MARK:- Future Order Date Time
//    
//    var strFutureDeliveryDate : String = ""
//    var strFutureDeliveryTime : String = ""
//    
//    //MARK:- Apply Coupon Code
//    
//    var strCouponCode : String = ""
//    
//    //MARK:- Confirm Order
//    
//    var isSelectPaymentOption : Bool = false
//    var strSelectPaymentOption : String = ""
//    
//    //MARK:- Select Store
//    
//    var strSelectStore : String = ""
//    var strOrderNumber : String = ""
    
    //MARK:- Change password
    
    var strOldPassword:String = ""
    var strNewPassword : String = ""
    var strConfirmPassword : String = ""
    
    //MARK:- Contact Us
    
    var strAddress:String = ""
    
    //MARK:- Registration
    
    var isTermsCondition:Bool = false
    
    //MARK:- Validation Function
    
    func isCheckValidForPickupStore() -> Bool
    {
        if (strName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_name_key")
            return false
        }
        else if (strPhoneNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_mobile_number_key")
            return false
        }
        else if strPhoneNumber.isNumeric == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_mobile_number_key")
            return false
        }
        else if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_email_address_key")
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_email_address_key")
            return false
        }
        return true
    }
    
    func isCheckValidForDeliveryStore() -> Bool
    {
        if (strName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_name_key")
            return false
        }
        else if (strPhoneNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_mobile_number_key")
            return false
        }
        else if strPhoneNumber.isNumeric == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_mobile_number_key")
            return false
        }
        else if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_email_address_key")
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_email_address_key")
            return false
        }
        /*else if (strUnitNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_unit_number_key")
            return false
        }
        else if strUnitNumber.isNumeric == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_only_digit_key")
            return false
        }*/
        else if (strStreetNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_street_number_key")
            return false
        }
        else if strStreetNumber.isNumeric == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_only_digit_key")
            return false
        }
        else if (strStreetName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_street_name_key")
            return false
        }
        else if(strSuburbName.trimmingCharacters(in: .whitespaces).isEmpty)
        {
            self.strValidationMessage = getValidationString(key: "Please_select_suburb_key")
            return false
        }
        return true
    }
    
    
    
    func isChangePassword() -> Bool{
        
        if (strOldPassword.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_your_old_password_key")
            return false
        }
        else if (strNewPassword.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_your_new_password_key")
            return false
        }
        else if strNewPassword.count < 6
        {
            self.strValidationMessage = getValidationString(key: "Password_must_be_at_least_6_characterslong_key")
            return false
        }
        else if strConfirmPassword == ""
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_confirm_password_key")
            return false
        }
        else if(!(strNewPassword == strConfirmPassword))
        {
            self.strValidationMessage = getValidationString(key: "Password_and_confirmation_password_must_match_key")
            return false
        }
        return true
    }
    
    
    func isCheckValidForContactUsStore() -> Bool
    {
        if (strName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_name_key")
            return false
        }
        else if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_email_address_key")
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_email_address_key")
            return false
        }
        else if (strPhoneNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_mobile_number_key")
            return false
        }
        else if strPhoneNumber.isNumeric == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_mobile_number_key")
            return false
        }
        else if (strAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_address_key")
            return false
        }
        
        return true
    }
    
    func isLogin() -> Bool {
        
        if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_email_address_key")
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_email_address_key")
            return false
        }
        else if (strOldPassword.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_your_password_key")
            return false
        }
        return true
    }
    
    
    func isRegistration() -> Bool {
        
        if (strName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_name_key")
            return false
        }
        else if (strPhoneNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_mobile_number_key")
            return false
        }
        else if strPhoneNumber.isNumeric == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_mobile_number_key")
            return false
        }
        else if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_email_address_key")
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_email_address_key")
            return false
        }
        else if (strOldPassword.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_your_password_key")
            return false
        }
        else if strOldPassword.count < 6
        {
            self.strValidationMessage = getValidationString(key: "Password_must_be_at_least_6_characterslong_key")
            return false
        }
        else if strConfirmPassword == ""
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_confirm_password_key")
            return false
        }
        else if(!(strOldPassword == strConfirmPassword))
        {
            self.strValidationMessage = getValidationString(key: "Password_and_confirmation_password_must_match_key")
            return false
        }
        else if isTermsCondition == false
        {
            self.strValidationMessage = getValidationString(key: "Please_terms_condition_key")
            return false
        }
        return true
    }
    
    func isEditProfile() -> Bool {
        
        if (strName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_name_key")
            return false
        }
        else if (strPhoneNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_mobile_number_key")
            return false
        }
        else if strPhoneNumber.isNumeric == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_mobile_number_key")
            return false
        }
        else if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_email_address_key")
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_email_address_key")
            return false
        }
        else if (strAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_address_key")
            return false
        }
        return true
    }
    
    //MARK:- Forgot Password
    func isCheckForgotEmailAddress() -> Bool
    {
        if (strEmailAddress.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_email_address_key")
            return false
        }
        else if strEmailAddress.isValidEmail() == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_valid_email_address_key")
            return false
        }
        return true
    }
    
    //MARK:- Credit Card Validation
    
    func isCheckCardDetail() -> Bool
    {
        if (strCardHolderName.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_card_holder_name_key")
            return false
        }
        else if (strCardNumber.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_card_number_name_key")
            return false
        }
        else if strCardNumber.isNumeric == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_only_digit_key")
            return false
        }
            /* else if strCardNumber.count < accountNumber
             {
             self.strValidationMessage = getValidationString(key: "Card_number_must_be_at_least_16_characterslong_key")
             return false
             }*/
        else if strExpiryMonth == ""
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_card_expiry_month_key")
            return false
        }
        else if strExpiryYear == ""
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_card_expiry_year_key")
            return false
        }
        else if (strCVV.trimmingCharacters(in: .whitespaces)).isEmpty
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_card_CVV_number_key")
            return false
        }
        else if strCVV.isNumeric == false
        {
            self.strValidationMessage = getValidationString(key: "Please_enter_only_digit_key")
            return false
        }
        else if strCVV.count < cvvNumber
        {
            self.strValidationMessage = getValidationString(key: "CVV_number_must_be_at_least_3_characterslong_key")
            return false
        }
        return true
    }
    
   
}

