//
//  UIView+Extension.swift
//  USteerTeacher
//
//  Created by om on 1/3/18.
//  Copyright © 2018 Dignizant. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }  
 
    
    func fadeIn(_ duration: TimeInterval = 0.2, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
            self.isHidden = false
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 0.2, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
           // self.isHidden = true

        }, completion: completion)
    }
    
    
    func Enter()
    {
        self.alpha = 1
        let top = CGAffineTransform(translationX: 0, y: 0)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            // self.container is your view that you want to animate
            self.transform = top
        }, completion: nil)
    }
    func Out()
    {
        let height = self.frame.size.height
        let bottom = CGAffineTransform(translationX: 0, y: -height)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            // self.container is your view that you want to animate
            self.transform = bottom
            
        }, completion:  { (finished: Bool) in
            self.alpha = 0
            self.entry()
        })
    }
    func entry()
    {
        let height = self.bounds.height
        let bottom = CGAffineTransform(translationX: 0, y: height+height)
        self.transform = bottom
        
        self.Enter()
    }
    func BackOut()
    {
        let height = self.frame.size.height
        let bottom = CGAffineTransform(translationX: 0, y: height+height)
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [], animations: {
            // Add the transformation in this block
            // self.container is your view that you want to animate
            self.transform = bottom
            
        }, completion:  { (finished: Bool) in
            self.alpha = 0
            self.BackEntry()
        })
    }
    func BackEntry()
    {
        let height = self.bounds.height
        let bottom = CGAffineTransform(translationX: 0, y: -height)
        self.transform = bottom
        
        self.Enter()
    }
    /*func convertViewToImage() -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, true, 0)
        self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        //        self.init(CGImage: image.CGImage!)
//        let data = UIImagePNGRepresentation(image!)
//        return UIImage(data: data!)!
        
        return image!
    }*/
    func convertViewToImage(carIcon : Int) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, true, 0)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let colorMasking : [CGFloat] = [222.0, 255.0, 222.0, 255.0, 222.0, 255.0]
        let imageRef = img?.cgImage?.copy(maskingColorComponents: colorMasking)
        let resultThumbImage = UIImage(cgImage: imageRef!, scale: 0, orientation: (img?.imageOrientation)!)
//        let finalimg = UIImage(cgImage: imageRef!)
        
        //TODO:- View size from createViewForPoint
        var finalImage  : UIImage?
        if(carIcon > 6)
        {
            finalImage = resizeImage(image: resultThumbImage, targetSize: CGSize(width:55, height:36))
        }else{
            finalImage = resizeImage(image: resultThumbImage, targetSize: CGSize(width:80, height:40))
        }
        
        return finalImage!
        
    }
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage
    {
        UIGraphicsBeginImageContextWithOptions(targetSize, false, 0.0);
        image.draw(in: CGRect(origin: CGPoint.zero, size: CGSize(width: targetSize.width, height: targetSize.height)))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    func createGradientLayer()
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [UIColor.white.withAlphaComponent(0.35).cgColor, UIColor.lightGray.withAlphaComponent(0.85).cgColor]
        self.layer.addSublayer(gradientLayer)
    }
    
    class func fromNib<T: UIView>() -> T {
            return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}
