//
//  UILabel + Extension.swift
//  PizzaApp
//
//  Created by DK on 09/10/17.
//  Copyright © 2017 Dignizant. All rights reserved.
//

import Foundation
import UIKit

extension UILabel {
    func addIconToLabel(imageName: UIImage?, labelText: String, bounds_x: Double, bounds_y: Double, boundsWidth: Double, boundsHeight: Double) {
        let attachment = NSTextAttachment()
        attachment.image = imageName
        attachment.bounds = CGRect(x: bounds_x, y: bounds_y, width: boundsWidth, height: boundsHeight)
        let attachmentStr = NSAttributedString(attachment: attachment)
        let string = NSMutableAttributedString(string: "")
        string.append(attachmentStr)
        let string2 = NSMutableAttributedString(string: labelText)
        string.append(string2)
        self.attributedText = string
    }
    func addImage(imageName: String, afterLabel bolAfterLabel: Bool = false , strText : String)
    {
        let attachment: NSTextAttachment = NSTextAttachment()
        attachment.image = UIImage(named: imageName)?.resizableImage(withCapInsets: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), resizingMode: .stretch)
        let attachmentString: NSAttributedString = NSAttributedString(attachment: attachment)
        
        if (bolAfterLabel)
        {
            let strLabelText: NSMutableAttributedString = NSMutableAttributedString(string:strText)
            strLabelText.append(attachmentString)
            
            self.attributedText = strLabelText
        }
        else
        {
            let strLabelText: NSAttributedString = NSAttributedString(string: strText)
            let mutableAttachmentString: NSMutableAttributedString = NSMutableAttributedString(attributedString: attachmentString)
            mutableAttachmentString.append(strLabelText)
            
            self.attributedText = mutableAttachmentString
        }
     }
    func addImageToLabel(imageName: UIImage, afterLabel bolAfterLabel: Bool = false , strText : String,width:Int,height:Int)
    {
        let attachment: NSTextAttachment = NSTextAttachment()
//        attachment.image = UIImage(named: imageName)?.resizableImage(withCapInsets: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), resizingMode: .stretch)
        attachment.image = resizeImage(image: imageName, targetSize: CGSize(width:width,height:height))
        let attachmentString: NSAttributedString = NSAttributedString(attachment: attachment)
        
        if (bolAfterLabel)
        {
            let strLabelText: NSMutableAttributedString = NSMutableAttributedString(string:strText)
            strLabelText.append(attachmentString)
            
            self.attributedText = strLabelText
        }
        else
        {
            let strLabelText: NSAttributedString = NSAttributedString(string: strText)
            let mutableAttachmentString: NSMutableAttributedString = NSMutableAttributedString(attributedString: attachmentString)
            mutableAttachmentString.append(strLabelText)
            
            self.attributedText = mutableAttachmentString
        }
    }
    func addTwoImage(imageName: String,imageName2: String, afterLabel bolAfterLabel: Bool = false , strText : String)
    {
        let attachment1: NSTextAttachment = NSTextAttachment()
        attachment1.image = UIImage(named: imageName)?.resizableImage(withCapInsets: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), resizingMode: .stretch)
        let attachmentString1 : NSAttributedString = NSAttributedString(attachment: attachment1)
        
        let attachment2: NSTextAttachment = NSTextAttachment()
        attachment2.image = UIImage(named: imageName2)?.resizableImage(withCapInsets: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), resizingMode: .stretch)
        let attachmentString2 : NSAttributedString = NSAttributedString(attachment: attachment2)
        
        if (bolAfterLabel)
        {
            let strLabelText: NSMutableAttributedString = NSMutableAttributedString(string:strText)
            self.attributedText = strLabelText
        }
        else
        {
            let strLabelText: NSAttributedString = NSAttributedString(string: strText)
            let mutableAttachmentString: NSMutableAttributedString = NSMutableAttributedString(attributedString: attachmentString1)
            let strLabelTextspace: NSMutableAttributedString = NSMutableAttributedString(string:" ")
            mutableAttachmentString.append(strLabelTextspace)
            mutableAttachmentString.append(attachmentString2)
            mutableAttachmentString.append(strLabelText)
            
            self.attributedText = mutableAttachmentString
        }
    }
    
    func setUpGlowLabel()
    {
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowRadius = 2.0
        self.layer.shadowOpacity = 1.0
        self.layer.masksToBounds = false
        self.layer.shouldRasterize = true
    }

    func startBlink()
    {
        UIView.animate(withDuration: 0.8,
                       delay:0.0,
                       options:[.allowUserInteraction, .curveEaseInOut, .autoreverse, .repeat],
                       animations: { self.alpha = 0 },
                       completion: nil)
    }
    
    func stopBlink() {
        layer.removeAllAnimations()
        alpha = 1
    }
}
