//
//  UIFont+Extension.swift
//  Telacoach
//
//  Created by om on 7/12/18.
//  Copyright © 2018 om. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String
{
    case regular = "Poppins-Regular"
    case medium = "Poppins-Medium"
    case semibold = "Poppins-SemiBold"
}

extension UIFont
{
    
}

func themeFont(size : Float,fontname : themeFonts) -> UIFont
{
    if UIScreen.main.bounds.width <= 320
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
    }
    else
    {
        return UIFont(name: fontname.rawValue, size: CGFloat(size))!
    }
    
}
