//
//  LoginVC.swift
//  Demo1
//
//  Created by om on 12/25/18.
//  Copyright © 2018 om. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class LoginVC: UIViewController {

    @IBOutlet var vwEmail : UIView!
    @IBOutlet var vwPassword : UIView!
    
    @IBOutlet var lblEmail : UILabel!
    @IBOutlet var lblPassword : UILabel!
    
    @IBOutlet var txtEmail : UITextField!
    @IBOutlet var txtPassword : UITextField!

    @IBOutlet var btnForgotPassword : UIButton!
    @IBOutlet var btnLogin : UIButton!
    @IBOutlet var btnSkip : UIButton!

    @IBOutlet var btnSignUp : UIButton!

    //MARK:- ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        SetupUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        sideMenuController?.isLeftViewSwipeGestureDisabled = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        sideMenuController?.isLeftViewSwipeGestureDisabled = true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
}
//MARK:- UI setUp
extension LoginVC
{
    func SetupUI()
    {
        [vwEmail,vwPassword].forEach { (view) in
            view?.layer.borderWidth = 1
            view?.layer.borderColor = UIColor.appThemeLightGrayColor.cgColor
            view?.layer.cornerRadius = 3
            view?.layer.masksToBounds = true
            
        }
    
        [lblEmail,lblPassword].forEach { (label) in
            label?.font = themeFont(size: 17, fontname: .regular)
            label?.textColor = UIColor.appThemeLightGrayColor
        }
        
        [txtEmail,txtPassword].forEach { (textfield) in
            textfield?.font = themeFont(size: 17, fontname: .regular)
            textfield?.textColor = UIColor.appThemeLightGrayColor
        }
        
        [btnLogin,btnSkip].forEach { (button) in
            button?.layer.borderWidth = 1
            button?.layer.borderColor = UIColor.appThemeLightGrayColor.cgColor
            button?.layer.cornerRadius = (button?.frame.size.height)! / 2
            button?.layer.masksToBounds = true
            button?.titleLabel?.font = themeFont(size: 20, fontname: .semibold)
            
        }
        btnLogin.backgroundColor = UIColor.appThemeRedColor
        btnSkip.backgroundColor = UIColor.clear
        
        btnSkip.layer.borderWidth = 1
        btnSkip.layer.borderColor = UIColor.appThemeRedColor.cgColor
        
        btnLogin.setTitleColor(UIColor.white, for: .normal)
        btnSkip.setTitleColor(UIColor.appThemeRedColor, for: .normal)
        
        btnForgotPassword.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        btnForgotPassword.setTitleColor(UIColor.appThemeLightGrayColor, for: .normal)
        
        btnSignUp.titleLabel?.font = themeFont(size: 14, fontname: .regular)
        btnSignUp.setTitleColor(UIColor.appThemeLightGrayColor, for: .normal)
        
        setUIForSignUpButton()
//        btnSignUp.setAttributedTitle(attributedString(string1: "Don't have an account yet?", string2: " Sign Up"), for: .normal)
        
        
    }
    func setUIForSignUpButton()
    {
        //
        let str1 = getCommonString(key: "Dont_have_account_key")
        let str2 = getCommonString(key: "Sign_up_key")
        
        let str = str1 + " " + str2
        let interactableText = NSMutableAttributedString(string:str)
//
        let rangeSignUp = (str as NSString).range(of: str2, options: .caseInsensitive)
        
        interactableText.addAttribute(NSAttributedString.Key.font,
                                      value: themeFont(size: 14, fontname: .regular),
                                      range: NSRange(location: 0, length: interactableText.length))
        
        interactableText.addAttribute(NSAttributedString.Key.foregroundColor,
                                      value: UIColor.appThemeLightGrayColor, range: NSRange(location: 0, length: interactableText.length))
        
        interactableText.addAttribute(NSAttributedString.Key.foregroundColor,
                                      value: UIColor.appThemeRedColor, range: rangeSignUp)
        
        btnSignUp.setAttributedTitle(interactableText, for: .normal)
        
    }
    func setEnableEmailUI()
    {
        vwEmail.layer.borderColor = UIColor.appThemeRedColor.cgColor
        lblEmail.textColor = UIColor.appThemeRedColor
        txtEmail.textColor = UIColor.appThemeRedColor
    }
    func setDisableEmailUI()
    {
        vwEmail.layer.borderColor = UIColor.appThemeLightGrayColor.cgColor
        lblEmail.textColor = UIColor.appThemeLightGrayColor
        txtEmail.textColor = UIColor.appThemeLightGrayColor
    }
    func setEnablePasswordUI()
    {
        vwPassword.layer.borderColor = UIColor.appThemeRedColor.cgColor
        lblPassword.textColor = UIColor.appThemeRedColor
        txtPassword.textColor = UIColor.appThemeRedColor
    }
    func setDisablePasswordUI()
    {
        vwPassword.layer.borderColor = UIColor.appThemeLightGrayColor.cgColor
        lblPassword.textColor = UIColor.appThemeLightGrayColor
        txtPassword.textColor = UIColor.appThemeLightGrayColor
    }
    func ResetDisableUI()
    {
        setDisableEmailUI()
        setDisablePasswordUI()
    }
}

//MARK:- Textfield methods
extension LoginVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        ResetDisableUI()
        if(textField == txtEmail)
        {
            setEnableEmailUI()
        }
        else if(textField == txtPassword)
        {
            setEnablePasswordUI()
        }
        return true
    }
}
//MARK:- Button Action
extension LoginVC
{
    @IBAction func btnSignUpAction(_ sender : UIButton)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RegisterVC") as? RegisterVC
        self.navigationController?.pushViewController(obj!, animated: true)
    }
    
    @IBAction func btnSkipAction(_ sender : UIButton)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        self.navigateUserWithLG(obj: obj)
    }
    
    @IBAction func btnForgotPasswordAction(_ sender : UIButton)
    {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as? ForgotPasswordVC
        self.navigationController?.pushViewController(obj!, animated: true)
    }
    
    @IBAction func btnLoginAction(_ sender : UIButton)
    {
        objUser = User()
        objUser.strEmailAddress = self.txtEmail.text ?? ""
        objUser.strOldPassword = self.txtPassword.text ?? ""
        
        if objUser.isLogin()
        {
            userLogin()
        }
        else
        {
            makeToast(message: objUser.strValidationMessage)
        }
    }
}

//MARK:- Service

extension LoginVC
{
    func userLogin()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kLogin)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "email_id":objUser.strEmailAddress,
                          "password":objUser.strOldPassword,
                          "device_token":"",
                          "register_id":"",
                          "device_type":strDeviceType]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        let data = json["data"]
                        guard let rowdata = try? data.rawData() else {return}
                        Defaults.setValue(rowdata, forKey: "userDetail")
                        Defaults.synchronize()
                        
                        
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.navigateUserWithLG(obj: obj)
                        
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}
