//
//  SideMenuVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 20/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class SideMenuVC: UIViewController {
    
    //MARK:- Variablde Declaration
    
    var arrSideMenu:[JSON] = []
    
     //MARK:- Outlet Zone
    
    @IBOutlet weak var btnUserNameOutlet: UIButton!
    @IBOutlet weak var btnLoginOutlet: UIButton!
    @IBOutlet weak var tblSideMenu: UITableView!
    @IBOutlet weak var heightOftblSideMenu: NSLayoutConstraint!
    
    //MARK:- Viewlife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        arrSideMenu = setupSideMenuArray()
        self.tblSideMenu.reloadData()
        self.tblSideMenu.tableFooterView = UIView()
        
        self.btnUserNameOutlet.setTitleColor(.white, for: .normal)
        self.btnUserNameOutlet.titleLabel?.font = themeFont(size: 22, fontname: .semibold)
        
        self.btnLoginOutlet.setTitleColor(UIColor.appThemeLightGrayColor, for: .normal)
        self.btnLoginOutlet.titleLabel?.font = themeFont(size: 15, fontname: .regular)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        if getUserDetail("username") != ""
        {
            self.btnUserNameOutlet.setTitle(getUserDetail("username"), for: .normal)
            self.btnLoginOutlet.setTitle(getCommonString(key: "Logout_key"), for: .normal)
            NotificationCenter.default.addObserver(self, selector: #selector(userLogout), name: NSNotification.Name(rawValue: "logoutAPI"), object: nil)
        }
        else
        {
            self.btnUserNameOutlet.setTitle(getCommonString(key: "Guest_user_key").capitalized, for: .normal)
            self.btnLoginOutlet.setTitle(getCommonString(key: "Login/register_key").capitalized, for: .normal)
        }
        
        tblSideMenu.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblSideMenu.removeObserver(self, forKeyPath: "contentSize")
    }
    
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblSideMenu.contentSize.height)")
            self.heightOftblSideMenu.constant = tblSideMenu.contentSize.height
        }
    }
    
    

}

//MARK:- Array Setup

extension SideMenuVC
{
    func setupSideMenuArray() -> [JSON]
    {
        var arrMenu:[JSON] = []
        
        var dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Home_key")
        dict["image"].stringValue = "ic_home_sidemenu"
        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Menu_key")
        dict["image"].stringValue = "ic_menu_sidemenu"
        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Tracking_key")
        dict["image"].stringValue = "ic_tracking_sidemenu"
        arrMenu.append(dict)
        
        if getUserDetail("username") != ""
        {
            dict = JSON()
            dict["name"].stringValue = getCommonString(key: "My_account_key")
            dict["image"].stringValue = "ic_my_account_sidemenu"
            arrMenu.append(dict)
        }
       
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "About_us_key")
        dict["image"].stringValue = "ic_about_us_sidemenu"
        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Contact_us_key")
        dict["image"].stringValue = "ic_contact_us_sidemenu"
        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Help_setting_key")
        dict["image"].stringValue = "ic_setting_sidemenu"
        arrMenu.append(dict)
        
        if getUserDetail("username") == ""
        {
            dict = JSON()
            dict["name"].stringValue = getCommonString(key: "Login_key")
            dict["image"].stringValue = "ic_login_sidemenu"
            arrMenu.append(dict)
        }
        
        return arrMenu
    }
}

//MARK:- Action Zone

extension SideMenuVC
{
    
    @IBAction func btnLoginAction(_ sender:UIButton)
    {
        if getUserDetail("username") != ""
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyAccountVC") as! MyAccountVC
            navigateUserWithLG(obj: obj)
        }
        
    }
    
    @IBAction func btnLogoutAction(_ sender:UIButton)
    {
        if getUserDetail("username") != ""
        {
            let alertController = UIAlertController(title: getCommonString(key: "Pizzaapp_key"), message: getCommonString(key: "Do_you_want_to_logout_from_the_app_key"), preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
               
                self.userLogout()
            }
            let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
                print("Cancel")
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            navigateUserWithLG(obj: obj)
        }
    }
}


//MARK:- Tablview Delegate & Datasource

extension SideMenuVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSideMenu.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuCell") as! SideMenuCell
        cell.setData(data: arrSideMenu[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = arrSideMenu[indexPath.row]
        if getCommonString(key: "Home_key") == dict["name"].stringValue
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            navigateUserWithLG(obj: obj)
        }
        else if getCommonString(key: "Tracking_key") == dict["name"].stringValue
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "TrackingVC") as! TrackingVC
            navigateUserWithLG(obj: obj)           
        }
        else if getCommonString(key: "My_account_key") == dict["name"].stringValue
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyAccountVC") as! MyAccountVC
            navigateUserWithLG(obj: obj)
        }
        else if getCommonString(key: "Contact_us_key") == dict["name"].stringValue
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "ContactUsVC") as! ContactUsVC
            navigateUserWithLG(obj: obj)
        }
        else if getCommonString(key: "Help_setting_key") == dict["name"].stringValue
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "HelpSettingVC") as! HelpSettingVC
            navigateUserWithLG(obj: obj)
        }
        else if getCommonString(key: "About_us_key") == dict["name"].stringValue
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommonPolicyVC") as! CommonPolicyVC
            obj.dict = dict
            obj.selectedPolicyType = .AboutUs
            navigateUserWithLG(obj: obj)
        }
        else if getCommonString(key: "Menu_key") == dict["name"].stringValue
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "MenuVC") as! MenuVC
            navigateUserWithLG(obj: obj)
        }
        else if getCommonString(key: "Login_key") == dict["name"].stringValue
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
            navigateUserWithLG(obj: obj)
        }
        
        
    }
}


//MARK:- Service

extension SideMenuVC
{
    @objc func userLogout()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kUserLogout)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "id":getUserDetail("id"),
                          "device_token":""]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        Defaults.removeObject(forKey: "userDetail")
                        Defaults.synchronize()
                        
                        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                        self.navigateUserWithLG(obj: obj)
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}


