//
//  MenuVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 29/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import AlamofireSwiftyJSON
import Alamofire
import SwiftyJSON
import SDWebImage
import LNICoverFlowLayout
import CHIPageControl


class MenuVC: UIViewController {

    
    
    //MARK:- Outlets
    @IBOutlet weak var collectionViewMenu: UICollectionView!
    @IBOutlet weak var flowLayout: LNICoverFlowLayout!
    @IBOutlet weak var pageControl: CHIPageControlJaloro!

    
    //MARK:- Variables
    var arrayMenu : [JSON] = []
    var originalItemSize = CGSize()
    var originalCollectionViewSize = CGSize()
    
    
    
    //MARK:- Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        collectionViewMenu.register(UINib(nibName: "MenuCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MenuCollectionViewCell")
        originalItemSize = flowLayout.itemSize
        originalCollectionViewSize = collectionViewMenu.bounds.size
        pageControl.isHidden = true
        ConfigurePageControl()
        getMenuService()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Menu_key").capitalized, type: .sidemenu, barType: .white)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(2 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC)) {
            self.collectionViewMenu.reloadData()
        }
        
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        flowLayout.invalidateLayout()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        flowLayout.itemSize = CGSize(width: collectionViewMenu.bounds.size.width * 0.80, height: collectionViewMenu.bounds.size.height)
        
        ConfigureFlowLayout()
        
        collectionViewMenu.setNeedsLayout()
        collectionViewMenu.layoutIfNeeded()
        collectionViewMenu.reloadData()
        
    }

    func ConfigureFlowLayout()
    {
        flowLayout.maxCoverDegree = 0
        flowLayout.coverDensity = 0.05
        flowLayout.minCoverScale = 0.81
        flowLayout.minCoverOpacity = 1.0
    }

    func ConfigurePageControl()
    {
        
        self.pageControl.progress = 0
        self.pageControl.currentPageTintColor = UIColor.appThemeRedColor
        self.pageControl.radius = 0
        self.pageControl.tintColor = UIColor.appThemeRedColor.withAlphaComponent(0.45)
        
    }
}
//MARK:- Scrollview methods
extension MenuVC
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let currentPage =  self.collectionViewMenu.contentOffset.x / self.collectionViewMenu.frame.size.width
        print("currentPage - ",currentPage)
        pageControl.progress = Double(currentPage)

    }
}

//MARK:- Collectionview Delegate & Datasource

extension MenuVC :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrayMenu.count

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCollectionViewCell", for: indexPath) as! MenuCollectionViewCell
        
        let dict = arrayMenu[indexPath.row]
        let strURL = dict["image"].stringValue
        cell.imgvwMenu.sd_setShowActivityIndicatorView(true)
        cell.imgvwMenu.sd_setIndicatorStyle(.gray)
        cell.imgvwMenu.sd_setImage(with: URL(string: strURL) , placeholderImage:UIImage(named:"banner_home_page.png"), options: .lowPriority, completed: nil)
        
//        cell.imgvwMenu.image = UIImage(named:"banner_home_page.png")
        cell.btnDownload.tag = indexPath.row
        cell.btnDownload.addTarget(self, action: #selector(btnDownloadAction), for: .touchUpInside)
        cell.lblStoreName.text = dict["store_name"].stringValue + "\n\(getCommonString(key: "Download_menu_key").capitalized)"

        return cell
    }
    
    /*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        print("width - ",collectionView.frame.width*0.75)
        return CGSize(width: collectionView.frame.width*0.75, height: collectionView.frame.height)
    }*/
}
//MARK:- Other methods
extension MenuVC
{
    @objc func btnDownloadAction(_ sender : UIButton)
    {
        showLoader()
        let dict = arrayMenu[sender.tag]
//        let img = UIImage(named:"banner_home_page.png")
        let strURL = dict["image"].stringValue

        
        /*if let url = URL(string: strURL),
            let data = try? Data(contentsOf: url),
            let image = UIImage(data: data) {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            makeToast(message: "Menu saved to albums successfully")

            stopLoader()
        }*/
        
        
        if !strURL.isEmpty
        {
            let imagePath = strURL
            Alamofire.request(imagePath, method: .get, parameters: [:], encoding: JSONEncoding.default)
                .validate { request, response, imageData in
                    if let downloadedImage = UIImage(data: imageData!) {
                        let image = downloadedImage
                        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                        makeToast(message: "Menu saved to albums successfully")
                    } else {
                        print(response)
                        print(imageData)
                    }
                    self.stopLoader()
                    return .success
            }
            
        }

        
    }
}




//MARK:- Service

extension MenuVC
{
    func getMenuService()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kGetMenu)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          
            ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrayMenu = json["data"].arrayValue
                        self.pageControl.numberOfPages = self.arrayMenu.count
                        self.pageControl.isHidden = false
                    }
                    else
                    {
                        self.arrayMenu = []
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.collectionViewMenu.reloadData()
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}

