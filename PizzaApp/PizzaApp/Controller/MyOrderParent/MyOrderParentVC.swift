//
//  MyOrderParentVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 25/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import CarbonKit

class MyOrderParentVC: UIViewController {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var viewOfMain: UIView!
    
    //MARK: - Varibales
    var carbonTabSwipeNavigation = CarbonTabSwipeNavigation()
    var items = NSArray()
    
    //MARK:- ViewLifeCycle  
    

    override func viewDidLoad() {
        super.viewDidLoad()

        items = [getCommonString(key: "Future_order_key").uppercased(),
                 getCommonString(key: "Past_order_key").uppercased()]

        carbonTabSwipeNavigation = CarbonTabSwipeNavigation(items: items as [AnyObject], delegate: self)
        carbonTabSwipeNavigation.insert(intoRootViewController: self, andTargetView: viewOfMain)
        
        style()
      
    }
    
    override func viewWillAppear(_ animated: Bool) {       
        
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "My_orders_key").capitalized, type: .back, barType: .white)
    }

}

//MARK: - Carbon kit
extension MyOrderParentVC:CarbonTabSwipeNavigationDelegate
{
    func style()
    {
        let width = UIScreen.main.bounds.size.width
        
        carbonTabSwipeNavigation.toolbarHeight.constant = 50
        
        let tabWidth = (width / CGFloat(items.count))
        let indicatorcolor: UIColor = UIColor.appThemeRedColor
        
        carbonTabSwipeNavigation.toolbar.isTranslucent = false
        
        carbonTabSwipeNavigation.setIndicatorColor(indicatorcolor)
        
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 0)
        carbonTabSwipeNavigation.carbonSegmentedControl!.setWidth(tabWidth, forSegmentAt: 1)        
        
        carbonTabSwipeNavigation.toolbar.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        carbonTabSwipeNavigation.toolbar.layer.shadowOffset = CGSize(width: 0, height: 2)
        carbonTabSwipeNavigation.toolbar.layer.shadowOpacity = 1.0
        carbonTabSwipeNavigation.toolbar.layer.masksToBounds = false
        
        carbonTabSwipeNavigation.carbonTabSwipeScrollView.isScrollEnabled = false
        carbonTabSwipeNavigation.setNormalColor(UIColor.appThemeLightGrayColor, font: themeFont(size: 17, fontname: .regular))
        carbonTabSwipeNavigation.setSelectedColor(UIColor.appThemeRedColor, font: themeFont(size: 17, fontname: .semibold))
    }
    
    func carbonTabSwipeNavigation(_ carbonTabSwipeNavigation: CarbonTabSwipeNavigation, viewControllerAt index: UInt) -> UIViewController
    {
        
        switch index
        {
        case 0:
            let vc = objStoryboard.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
            vc.selectedHistoryType = selectMyOrderType.future
            return vc
        case 1 :
            let vc = objStoryboard.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
            vc.selectedHistoryType = selectMyOrderType.past
            return vc
            
        default:
            let vc = objStoryboard.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
           vc.selectedHistoryType = selectMyOrderType.future
            return vc
        }
    }
    
}

