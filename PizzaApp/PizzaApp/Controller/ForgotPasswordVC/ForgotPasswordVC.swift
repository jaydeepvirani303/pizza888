//
//  ForgotPasswordVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 26/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON

class ForgotPasswordVC: UIViewController {
    
    //MARK:- Outlet Zone
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblTitleMessage: UILabel!
    @IBOutlet weak var lblEmailTitle: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSendOutlet: CustomButton!
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        // Do any additional setup after loading the view.
    }

}

//MARK:- Setup

extension ForgotPasswordVC
{
    func setupUI()
    {
        [lblTitle].forEach { (label) in
            label?.font = themeFont(size: 24, fontname: .semibold)
            label?.textColor = UIColor.appThemeRedColor
        }
        
        lblTitle.text = getCommonString(key: "Forgot_password_key").capitalized
        
        [txtEmail].forEach { (textfield) in
            textfield?.font = themeFont(size: 15, fontname: .regular)
            textfield?.textColor = UIColor.black
            textfield?.delegate = self
            textfield?.placeholder = getCommonString(key: "Enter_here_key")
        }
        
        [lblTitleMessage].forEach { (label) in
            label?.font = themeFont(size: 15, fontname: .medium)
            label?.textColor = UIColor.appThemeDarkGrayColor
        }
        
        lblTitleMessage.text = getCommonString(key: "Enter_your_register_email_id_to_recover_password_key")
        
        [lblEmailTitle].forEach { (label) in
            label?.textColor = UIColor.black
            label?.font = themeFont(size: 15, fontname: .regular)
        }
        
        lblEmailTitle.text = getCommonString(key: "Email_key").uppercased()
        
        setUpThemeButtonUI(button: btnSendOutlet)
        btnSendOutlet.setTitle(getCommonString(key: "Send_key").uppercased(), for: .normal)
    }
}

//MARK:- Textfield methods
extension ForgotPasswordVC : UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
    }
   
}

//MARK:- Action Zone

extension ForgotPasswordVC
{
    @IBAction func btnSendAction(_ sender:UIButton)
    {
        
        ForgotPasswordService()
    }
    
    @IBAction func btnBackAction(_ sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- Service

extension ForgotPasswordVC
{
    func ForgotPasswordService()
    {
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kForgotPassword)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "email_id":txtEmail.text ?? ""
            ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        makeToast(message: json["msg"].stringValue)
                        self.navigationController?.popViewController(animated: true)
                    }
                    else
                    {
                        
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}

