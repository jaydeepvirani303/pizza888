//
//  SelectItemChildVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 28/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import RealmSwift
import SDWebImage

class SelectItemChildVC: UIViewController {
    
    //MARK:- Variablde declaration
    
    var typeDD = DropDown()
    var baseDD = DropDown()

    var arrSelectItemList: [JSON]?
    var strMsg = String()
    var dictProduct = JSON()
    var selectedCategoryItem = selectCategoryItem.regular
    let upperRefresh = UIRefreshControl()
    
    //MARK:- Outlet ZOne
    
    @IBOutlet weak var tblSelectItem: UITableView!
    
    //MARK:- View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        upperRefresh.addTarget(self, action: #selector(self.upperRefreshTable), for: .valueChanged)
        tblSelectItem.addSubview(upperRefresh)
        setUpQuestionDropDown()
        setUpSelectBaseDropDown()
        upperRefreshTable()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tblSelectItem.reloadData()
    }
    
    @objc func upperRefreshTable()
    {
        getProduct()
        
    }
    

}



//MARK:- TablView Delegate & Datasource

extension SelectItemChildVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrSelectItemList == nil{
            return 10
        }
        if arrSelectItemList?.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMsg
            lbl.font = themeFont(size: 20, fontname: .medium)
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrSelectItemList!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectItemTableCell") as! SelectItemTableCell
        [cell.btnAddOutlet,cell.btnCustomizeSelectOutlet,cell.btnMinusOutlet,cell.btnSelectTypeOutlet,cell.btnAddProductOutlet].forEach { (btn) in
            btn?.tag = indexPath.row
        }
        if arrSelectItemList != nil{
            let dict = arrSelectItemList![indexPath.row]
            
            
            cell.imgProduct.sd_setShowActivityIndicatorView(true)
            cell.imgProduct.sd_setIndicatorStyle(.gray)
            cell.imgProduct.sd_setImage(with: URL(string: dict["image"].stringValue) , placeholderImage:UIImage(named:"ic_placeholder"), options: .lowPriority, completed: nil)
            
            switch selectedCategoryItem
            {
            case .drink:
                let cell = tableView.dequeueReusableCell(withIdentifier: "DrinkCell") as! DrinkCell               
                cell.setData(dict: dict)
                cell.btnAddProductOutlet.tag = indexPath.row
                cell.btnAddProductOutlet.addTarget(self, action: #selector(btnAddProductAction), for: .touchUpInside)
                cell.imgvwProduct.sd_setShowActivityIndicatorView(true)
                cell.imgvwProduct.sd_setIndicatorStyle(.gray)
                cell.imgvwProduct.sd_setImage(with: URL(string: dict["image"].stringValue) , placeholderImage:UIImage(named:"ic_placeholder"), options: .lowPriority, completed: nil)
                cell.btnCustomizeSelectOutlet.tag = indexPath.row
                return cell
               
            case .regular:
                cell.vwSelectBase.isHidden = true
                cell.vwCustomizeContent.isHidden = false
                cell.btnAddProductOutlet.tag = indexPath.row
                cell.btnAddProductOutlet.addTarget(self, action: #selector(btnAddProductAction), for: .touchUpInside)
                
                cell.vwType.isHidden = true
                cell.vwHidden.isHidden = false
                cell.btnCustomizeSelectOutlet.tag = indexPath.row
                /*if(dictProduct["isTypePasta"].stringValue == "1")
                {
                    cell.vwType.isHidden = true
                    cell.vwHidden.isHidden = false
                }else{
                    cell.vwType.isHidden = false
                    cell.vwHidden.isHidden = true
                }*/
                

                break
//            case .pasta:
//                cell.vwHidden.isHidden = true
//                cell.vwCustomizeContent.isHidden = true
//                cell.vwSelectBase.isHidden = false
//                cell.lblBase.text = dict["pastaBase"].stringValue
//                cell.btnAddProductOutlet.tag = indexPath.row
//                cell.btnCustomizeSelectOutlet.tag = indexPath.row
//                cell.btnCustomizeSelectOutlet.addTarget(self, action: #selector(btnCustomizeSelectOutletAction), for: .touchUpInside)
//                cell.btnAddProductOutlet.addTarget(self, action: #selector(btnAddProductAction), for: .touchUpInside)

            case .deal:
                let cell = tableView.dequeueReusableCell(withIdentifier: "DealCell") as! DealCell
                cell.stopAnimationSkeleton()
                cell.imgvwProduct.sd_setShowActivityIndicatorView(true)
                cell.imgvwProduct.sd_setIndicatorStyle(.gray)
                cell.imgvwProduct.sd_setImage(with: URL(string: dict["image"].stringValue) , placeholderImage:UIImage(named:"ic_placeholder"), options: .lowPriority, completed: nil)
                cell.setDataForDeal(dict: dict)
                cell.btnCustomizeSelectOutlet.tag = indexPath.row
                return cell
            case .half:
                let cell = tableView.dequeueReusableCell(withIdentifier: "DealCell") as! DealCell
                cell.imgvwProduct.sd_setShowActivityIndicatorView(true)
                cell.imgvwProduct.sd_setIndicatorStyle(.gray)
                cell.imgvwProduct.sd_setImage(with: URL(string: dict["image"].stringValue) , placeholderImage:UIImage(named:"ic_placeholder"), options: .lowPriority, completed: nil)
                cell.stopAnimationSkeleton()
                cell.setDataForHalf(dict: dict)
                cell.btnCustomizeSelectOutlet.tag = indexPath.row
                return cell
                
            }
            
            cell.stopAnimationSkeleton()
            cell.setData(dict: dict)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if arrSelectItemList?.count == 0
        {
            return
        }
        
        switch selectedCategoryItem
        {
        case .drink:
            break
        case .regular:
            break
//        case .pasta:
//            break
        case .deal:
            let obj = objStoryboard.instantiateViewController(withIdentifier: "DealDetailVC") as! DealDetailVC
            obj.dictData = arrSelectItemList![indexPath.row]
            self.navigationController?.pushViewController(obj, animated: true)
        case .half:
            let obj = objStoryboard.instantiateViewController(withIdentifier: "HalfParentVC") as! HalfParentVC
            obj.dictData = arrSelectItemList![indexPath.row]
            self.navigationController?.pushViewController(obj, animated: true)
            break
            
        }
        
      
    }
    
    
}

//MARK:- Setup DropDown

extension SelectItemChildVC
{
    func setUpQuestionDropDown()
    {
        typeDD.dataSource = getPizzaTypeNames()
        typeDD.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            print("Cell \(self.typeDD.tag) clicked with \(item)")
            self.typeDD.hide()
            
            if let cell = self.tblSelectItem.cellForRow(at: IndexPath(row: self.typeDD.tag, section: 0)) as? SelectItemTableCell
            {
                var dict = self.arrSelectItemList![self.typeDD.tag]
                var id = ""
//                var price : Float = 0.0
                if(setUpPizzaType().contains { (json) -> Bool in
                    if json["typeName"].stringValue == item
                    {
                        id = json["typeId"].stringValue
                        dict["pizzaSize"].stringValue = json["typeName"].stringValue
                        return true
                    }
                    return false
                })
                {
                    
                }
                
                dict["defaultTypeId"].stringValue =  id
                self.arrSelectItemList![self.typeDD.tag] = dict
                
                cell.setUpPrice(dict:dict)
            }
        }
        
    }
    func setUpSelectBaseDropDown()
    {
        baseDD.dataSource = getPastaBaseNames()
        baseDD.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            print("Cell \(self.baseDD.tag) clicked with \(item)")
            self.baseDD.hide()
            
            if let cell = self.tblSelectItem.cellForRow(at: IndexPath(row: self.baseDD.tag, section: 0)) as? SelectItemTableCell
            {
                var dict = self.arrSelectItemList![self.baseDD.tag]
                dict["pastaBase"].stringValue = item
                self.arrSelectItemList![self.baseDD.tag] = dict
                cell.lblBase.text = item
            }
        }
        
    }
    
}


//MARK:- Action Zone

extension SelectItemChildVC
{
    @objc func btnCustomizeSelectOutletAction(_ sender:UIButton)
    {
        typeDD.dataSource = getPastaSizeNames()
        typeDD.tag = sender.tag
        typeDD.show()
    }
    @IBAction func btnSelectTypeAction(_ sender:UIButton)
    {       
        configureDropdown(dropdown: typeDD, sender: sender)
        typeDD.tag = sender.tag
        typeDD.show()
    }
    @IBAction func btnSelectBaseAction(_ sender:UIButton)
    {
        configureDropdown(dropdown: baseDD, sender: sender)
        baseDD.dataSource = getPastaBaseNames()
        baseDD.tag = sender.tag
        baseDD.show()
    }
    @IBAction func btnPlusAction(_ sender:UIButton)
    {
        addItemToCart(tag: sender.tag)
    }
    
    @IBAction func btnMinusAction(_ sender:UIButton)
    {
        var dict = self.arrSelectItemList![sender.tag]
        if dict["quantity"].intValue == 0
        {
            return
        }
        let qty = dict["quantity"].intValue - 1
        dict["quantity"].stringValue = String(qty)
        self.arrSelectItemList![sender.tag] = dict
        self.tblSelectItem.reloadRows(at: [IndexPath(row: sender.tag, section: 0)], with: .none)
    }
    
    @IBAction func btnCustomizeAction(_ sender:UIButton)
    {
        if arrSelectItemList == nil{
            return
        }
        let obj = objStoryboard.instantiateViewController(withIdentifier: "CustomizeVC") as! CustomizeVC
        obj.addCartDelegate = self
        obj.dictData = arrSelectItemList![sender.tag]
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK:- Private ZOne

extension SelectItemChildVC
{
    func addItemToCart(tag:Int)
    {
        var dict = self.arrSelectItemList![tag]
        let qty = dict["quantity"].intValue + 1
        dict["quantity"].stringValue = String(qty)
        self.arrSelectItemList![tag] = dict
        self.tblSelectItem.reloadRows(at: [IndexPath(row: tag, section: 0)], with: .none)
        
        ////------- Realm Data save
        /*let PKId = cartIncrementID()
        let objCart = Cart().storeData(dict: dict,PKId : PKId)
        try! realm.write
        {
            realm.add(objCart)
        }*/
        
    }
    
//    func cartIncrementID() -> Int
//    {
//        print("cart id - ",Cart.primaryKey())
//        return (realm.objects(Cart.self).max(ofProperty: Cart.primaryKey()) as Int? ?? 0) + 1
//    }
//    
    func addAdditionalData(type:selectCategoryItem)
    {
        switch self.selectedCategoryItem
        {
        case .drink:
            for i in 0..<self.arrSelectItemList!.count
            {
                var dict = self.arrSelectItemList![i]
                dict["category_id"] = JSON(dictProduct["category_id"])
                dict["category_name"] = JSON(dictProduct["category_name"])
                self.arrSelectItemList![i] = dict
            }
            break
        /*case .pasta:
            let arrayPastaBase = getPastaBaseNames()
            for i in 0..<self.arrSelectItemList!.count
            {
                var dict = self.arrSelectItemList![i]
                if(arrayPastaBase.count > 0)
                {
                    dict["pastaBase"].stringValue = arrayPastaBase.first ?? "Select Base"
                    dict["defaultTypeId"].stringValue = "1"
                    dict["category_id"] = JSON(dictProduct["category_id"])
                    dict["category_name"] = JSON(dictProduct["category_name"])

                }
                self.arrSelectItemList![i] = dict
            }
            break*/
        case .regular:

            for i in 0..<self.arrSelectItemList!.count
            {
                var dict = self.arrSelectItemList![i]
               
                dict["sauce_type"].stringValue =  ""
                dict["pizzaSize"].stringValue = ""
                dict["extra_description"].stringValue =  ""
//                dict["topping"].stringValue = ""
                dict["is_customized"].stringValue = "0"
                
                if(dictProduct["isTypePasta"].stringValue == "1")
                {
                    dict["defaultTypeId"].stringValue = ""
                }else{
                    dict["defaultTypeId"].stringValue = "1"
                }
                
                dict["category_id"] = JSON(dictProduct["category_id"])
                dict["category_name"] = JSON(dictProduct["category_name"])
                self.arrSelectItemList![i] = dict
            }
            break
        case .deal:
            for i in 0..<self.arrSelectItemList!.count
            {
                var dict = self.arrSelectItemList![i]               
                dict["is_customized"].stringValue = "0"
                dict["category_id"] = JSON(dictProduct["category_id"])
                dict["category_name"] = JSON(dictProduct["category_name"])
//                dict["defaultTypeId"].stringValue = "1"
//                dict["sauce_type"].stringValue =  ""
//                dict["pizzaSize"].stringValue = ""
//                dict["extra_description"].stringValue =  ""
                self.arrSelectItemList![i] = dict
            }
        case .half:
            for i in 0..<self.arrSelectItemList!.count
            {
                var dict = self.arrSelectItemList![i]
                dict["is_customized"].stringValue = "0"
                dict["type"].stringValue = "4"
                dict["defaultTypeId"].stringValue = "1"
                dict["sauce_type"].stringValue =  ""
                dict["pizzaSize"].stringValue = ""
                dict["extra_description"].stringValue =  ""
                dict["category_id"] = JSON(dictProduct["category_id"])
                dict["category_name"] = JSON(dictProduct["category_name"])
                self.arrSelectItemList![i] = dict
            }
            
        }
        
    }
  
}
//MARK:- Other Methods
extension SelectItemChildVC
{
    @objc func btnAddProductAction(_ sender : UIButton)
    {
        var dict = arrSelectItemList?[sender.tag]
        dict?["customized"].stringValue = "0"

        let selectedType = dict?["type"].intValue
        
        if(selectedType == 2 || selectedType == 1 )
        {
            dict?["baseType"].stringValue = ""
            dict?["remove_ingr_id"].stringValue = ""
            dict?["remove_ingr_name"].stringValue = ""
            dict?["ingr_name"].stringValue = ""
            dict?["description"].stringValue = ""
            dict?["addedTopingsPrice"].stringValue = ""
            dict?["finalPrice"].stringValue = getPriceFormatedValue(strPrice: getPrice(dict: (arrSelectItemList?[sender.tag])!))
            dict?["quantity"].stringValue = "1"
            var strVariant = [String]()
            var price = self.getPrice(dict: dict!)
            if let arrSubType = dict?["sub_type"].arrayValue,arrSubType.count != 0 {
                for i in 0..<arrSubType.count {
                    let arrData = arrSubType[i]["data"].arrayValue
                    if arrData.count != 0 {
                        price += arrData[0]["value"].floatValue
                        strVariant.append(arrData[0]["crtName"].stringValue)
                    }
                }
            }
            dict?["finalPrice"].stringValue = getPriceFormatedValue(strPrice: price)
            dict?["addedVariant"].stringValue = strVariant.count != 0 ? strVariant.joined(separator: ",") : ""
        }
        AddToCart(dict : dict ?? JSON())
        /*let arrayCart = GetCartData()
        if arrayCart.count == 0 {
            AddToCart(dict : dict ?? JSON())
        } else {
            let arrFilter = arrayCart.filter({$0["store_id"].stringValue == objOrder.strSelectedStoreId})
            if arrFilter.count == 0 {
                deleteAllItemsfromCart()
            }
            AddToCart(dict : dict ?? JSON())
        }*/
    }
    
    
}
//MARK:- Delegate

extension SelectItemChildVC:delegateToAddCart
{
    func setDelegateToAddCart(dict: JSON)
    {
        for i in 0..<arrSelectItemList!.count
        {
            var dictData = arrSelectItemList![i]
            if dictData["product_id"].stringValue == dict["product_id"].stringValue
            {
                dictData["is_customized"].stringValue = "1"
                arrSelectItemList![i] = dictData
                addItemToCart(tag: i)
                break
            }
        }
    }
}


//MARK:- Service

extension SelectItemChildVC
{
    func getProduct()
    {
        upperRefresh.endRefreshing()
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kGetProduct)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "type":dictProduct["type"].stringValue,
                          "category_id":dictProduct["category_id"].stringValue,
                          "store_id": dictStoreDetail["id"].stringValue,
                          "display_product_type":strProductType,
                          "service_type":strServiceType
            ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrSelectItemList = []
                        self.arrSelectItemList = json["data"].arrayValue
                        self.addAdditionalData(type: self.selectedCategoryItem)
                        
                    }
                    else
                    {
                        self.strMsg = json["msg"].stringValue
                        self.arrSelectItemList = []
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblSelectItem.reloadData()
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}

