//
//  CheckOutVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 22/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class CheckOutVC: UIViewController {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblCheckOut: UITableView!
    @IBOutlet weak var lblTotalTitle: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    @IBOutlet weak var lblDeliveryTitle: UILabel!
    @IBOutlet weak var lblDeliveryAmount: UILabel!
    @IBOutlet weak var lblSubTotalTitle: UILabel!
    @IBOutlet weak var lblSubTotalAmount: UILabel!
    @IBOutlet weak var lblDiscountTitle: UILabel!
    @IBOutlet weak var lblDiscountAmount: UILabel!
    @IBOutlet weak var lblCheckOutTitle: UILabel!
    @IBOutlet weak var txtCoupon: UITextField!
    @IBOutlet weak var btnApplyOutlet: CustomButton!
    @IBOutlet weak var bottomOfview: NSLayoutConstraint!
    @IBOutlet weak var vwDiscount: UIView!
    @IBOutlet weak var vwDelivery: UIView!
    
    //MARK:- Variables
    var arrayCart : [JSON] = []
    var strDiscountAmount:String = ""
    var dictDiscount = JSON()
    var CheckUser: [CheckoutUser] = []
    
    //MARK:- Viewlife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        CheckUser = Array(globalRealm.objects(CheckoutUser.self))
        
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        setUpCartData()
        
        if arrayCart.count > 0{
            setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Checkout_key"), type: .clearAll, barType: .white)
        } else{
            setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Checkout_key"), type: .back, barType: .white)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SetTotalPriceValue"), object: nil)
        }
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func btnDeleteAllCartAction(_ sender: UIButton) {
        let alertController = UIAlertController(title: getCommonString(key: "Pizzaapp_key"), message: getCommonString(key: "Are_you_sure_you_want_to_remove_item_key"), preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.deleteAllItemsfromCart()
            self.setUpCartData()
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
            self.viewWillAppear(false)
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

extension CheckOutVC
{
    
    @objc func keyboardWillShow(_ notification: NSNotification)
    {
        // Do something here
        if let keyboardRectValue = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size
        {
            let keyboardHeight = keyboardRectValue.height
            print("keyBorad Height : \(keyboardHeight) ")
            UIView.animate(withDuration: 0.5, animations: {
                self.bottomOfview.constant = keyboardHeight
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification){
        
        UIView.animate(withDuration: 0.5, animations: {
            self.bottomOfview.constant = 0.0
            self.view.layoutIfNeeded()
            
        }, completion: nil)
    }
}

//MARK:- Setup UI
extension CheckOutVC
{
    func setupUI()  {
        
        [lblTotalAmount,lblTotalTitle,lblDeliveryTitle,lblDeliveryAmount,lblSubTotalTitle,lblSubTotalAmount,lblDiscountTitle,lblDiscountAmount].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .semibold)
            lbl?.textColor = UIColor.white
        }
        
        [lblCheckOutTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .regular)
            lbl?.textColor = UIColor.white
        }
        
        [txtCoupon].forEach { (txtField) in
            txtField?.font = themeFont(size: 15, fontname: .regular)
            txtField?.textColor = .black
            txtField?.delegate = self
        }
        
        txtCoupon.placeholder = getCommonString(key: "Voucher_code_key").capitalized
        
        [btnApplyOutlet].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .regular)
            btn?.setTitle(getCommonString(key: "Apply_key").uppercased(), for: .normal)
            btn?.setTitleColor(.white, for: .normal)
        }
        
        lblCheckOutTitle.text = getCommonString(key: "Checkout_key").uppercased()
        
        lblSubTotalTitle.text = getCommonString(key: "Total_key").uppercased()
        lblTotalTitle.text = getCommonString(key: "Subtotal_key").uppercased()
        lblDeliveryTitle.text = getCommonString(key: "Delivery_key").uppercased()
        lblDiscountTitle.text = getCommonString(key: "Discount_key").uppercased()
        
        let objCheckOutUser = CheckUser[0]
        self.vwDelivery.isHidden = objCheckOutUser.strSuburbCost == "" ? true : false
//        setUpCartData()
    }
}


//MARK:- UITextfiled Delegate

extension CheckOutVC :UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}

//MARK:- Setup cart data
extension CheckOutVC
{
    func setUpCartData()
    {
        arrayCart = []
        arrayCart = GetCartData()
        tblCheckOut.reloadData()
//
        if arrayCart.count == 0{
            setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Checkout_key"), type: .back, barType: .white)
            [self.txtCoupon].forEach({ (txtField) in
                txtField?.alpha = 0.5
                txtField?.isUserInteractionEnabled = false
                txtField?.text = ""
            })
            [self.btnApplyOutlet].forEach({ (btn) in
                btn?.alpha = 0.5
                btn?.isUserInteractionEnabled = false
                btn?.setTitle(getCommonString(key: "Apply_key").uppercased(), for: .normal)
            })
            lblSubTotalAmount.text = "\(strCurrenrcy) 0.00"
            lblTotalAmount.text = "\(strCurrenrcy) 0.00"
            self.vwDelivery.isHidden = true
            self.vwDiscount.isHidden = true
        } else {
             SetTotalPrice()
        }
    }
    
    func SetTotalPrice()
    {
        let totalPrice  = CartTotalPrice()
        
        lblTotalAmount.text = "\(strCurrenrcy) " + getPriceFormatedValue(strPrice: totalPrice)
        
        if totalPrice == 0.0{
            vwDelivery.isHidden = true
            lblSubTotalAmount.text = "\(strCurrenrcy) 0.00"
            return
        }
        
        let objCheckOutUser = CheckUser[0]
        
        lblDeliveryAmount.text = "\(strCurrenrcy) \(objCheckOutUser.strSuburbCost == "" ? "0.0" : objCheckOutUser.strSuburbCost)"
        
        let subTotal = totalPrice + Float(objCheckOutUser.strSuburbCost == "" ? "0.0" : objCheckOutUser.strSuburbCost)!
        
        lblSubTotalAmount.text = "\(strCurrenrcy) " + getPriceFormatedValue(strPrice: subTotal)
        
        if dictDiscount.count != 0{
//            setupPriceAfterApplyCouponCode(dict: dictDiscount)
            let dict = GetCheckOutCartItem()
            applyCouponCode(json: dict)
        }
    }
    
    func setupPriceAfterApplyCouponCode(dict:JSON) {
        let totalPrice  = CartTotalPrice()
        let disAmnt = dict["disamt"].floatValue
        
        vwDiscount.isHidden = false
        
        lblTotalAmount.text = "\(strCurrenrcy) " + getPriceFormatedValue(strPrice: totalPrice)
        
        let objCheckOutUser = CheckUser[0]
        
        lblDeliveryAmount.text = "\(strCurrenrcy) \(objCheckOutUser.strSuburbCost == "" ? "0.0" : objCheckOutUser.strSuburbCost)"
        
        let subTotal = totalPrice + Float(objCheckOutUser.strSuburbCost == "" ? "0.0" : objCheckOutUser.strSuburbCost)! - disAmnt
        
        lblSubTotalAmount.text = "\(strCurrenrcy) " + getPriceFormatedValue(strPrice: subTotal)
        
        lblDiscountAmount.text = "\(strCurrenrcy) " + getPriceFormatedValue(strPrice: disAmnt)
    }
    
    func setUpPrice(dict:JSON) -> Float
    {
        let id = dict["defaultTypeId"].stringValue
        var addedPrice : Float = 0.0
        
        switch id
        {
            case "0":
                addedPrice = dict["half_price"].floatValue
            case "1":
                addedPrice = dict["price"].floatValue
            case "2":
                addedPrice = dict["small_price"].floatValue
            default:
                addedPrice = dict["price"].floatValue
        }
        let toppingPrice = dict["addedTopingsPrice"].floatValue
        addedPrice = addedPrice + toppingPrice

        print("addedPrice - ",addedPrice)
        return addedPrice
        
    }
}



//MARK:- Tableview Delegate & Datasource

extension CheckOutVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayCart.count == 0
        {
            let lbl = UILabel()
            lbl.text = getCommonString(key: "No_record_found_key")
            lbl.font = themeFont(size: 20, fontname: .medium)
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrayCart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckOutCell") as! CheckOutCell
        [cell.btnDeleteOutlet,cell.btnAddOutlet,cell.btnMinusOutlet].forEach { (btn) in
            btn?.tag = indexPath.row
        }
        
        let dict = arrayCart[indexPath.row]
        cell.setUpData(dict: dict)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
//MARK:- Other methods
extension CheckOutVC
{
    func RemoveCartItem(index:Int)
    {
        let dict = arrayCart[index]
        
        DeleteCartItems(cartId: dict["cart_id"].intValue,type:dict["type"].intValue)
        
        setUpCartData()
    }
    
    /*func getCartItem()
    {
        var dictCheckoutJson = JSON()
        
        var arrayDrinkItems : [JSON] = []
        var arrayFullPizzaItems : [JSON] = []
        var arrayDealItems : [JSON] = []
        var arrayHalfHalfItems : [JSON] = []
        
        
        let arrayDirectAddedCartItems = Array(globalRealm.objects(Cart.self)).filter { $0.product_type == "1"}
        if(arrayDirectAddedCartItems.count > 0)
        {
            arrayDirectAddedCartItems.forEach { (objCart) in
                arrayDrinkItems.append(objCart.GetData())
            }
            
        }
        
        let arrayPizzaItems = Array(globalRealm.objects(Cart.self)).filter { $0.product_type == "2"}
        if(arrayPizzaItems.count > 0)
        {
            arrayPizzaItems.forEach { (objCart) in
                
                let strVarient = "\(objCart.pizzaSize),\(objCart.product_sauce_type),\(objCart.product_baseType)"
                var dictPizza = JSON()
                dictPizza["prdname"] = JSON(objCart.product_name)
                dictPizza["prdid"] = JSON(objCart.product_id)
                dictPizza["qty"] = "1"
                dictPizza["prdingrd"] = JSON(objCart.ingr_name)
                dictPizza["removetop"] = JSON(objCart.remove_ingr_name)
                //                dictPizza["pbase"] = JSON(objCart.product_baseType)
                //                dictPizza["psaurce"] = JSON(objCart.product_sauce_type)
                //                dictPizza["psize"] = JSON(objCart.pizzaSize)
                dictPizza["comment"] = JSON(objCart.product_description)
                dictPizza["variant"] = JSON(strVarient)
                
                if objCart.isCustomized == "1" {
                    dictPizza["prdvalue"] = JSON(getPriceFormatedValue(strPrice: setUpPrice(objCart: objCart)))
                } else {
                    dictPizza["prdvalue"] = JSON(objCart.product_price)
                }
                arrayFullPizzaItems.append(dictPizza)
            }
            
        }
        
        let arrayDeals = Array(globalRealm.objects(Cart.self)).filter { $0.product_type == "3"}
        if(arrayDeals.count > 0)
        {
            arrayDeals.forEach { (objCart) in
                
                var dictPizza = JSON()
                dictPizza["prdname"] = JSON(objCart.deal_name)
                dictPizza["prdid"] = JSON(objCart.deal_id)
                dictPizza["prdvalue"] = JSON(objCart.deal_price)
                dictPizza["qty"] = "1"
                dictPizza["rmingr"] = ""
                
                var arraySubData : [JSON] = []
                
                let arrayDealsSubItems = Array(globalRealm.objects(CartDealItems.self)).filter { $0.cart_id == objCart.id}
                if(arrayDealsSubItems.count > 0)
                {
                    
                    arrayDealsSubItems.forEach({ (objCartDealItemsLocal) in
                        
                        var dictPizza = JSON()
                        dictPizza["prdname"] = JSON(objCartDealItemsLocal.product_name)
                        dictPizza["prdid"] = JSON(objCartDealItemsLocal.product_id)
                        dictPizza["qty"] = "1"
                        dictPizza["prdvalue"] = JSON(objCartDealItemsLocal.product_price)
                        dictPizza["prdingrd"] = JSON(objCartDealItemsLocal.ingr_name)
                        dictPizza["removetop"] = JSON(objCartDealItemsLocal.remove_ingr_name)
                        //                        dictPizza["pbase"] = JSON(objCartDealItemsLocal.product_baseType)
                        //                        dictPizza["psaurce"] = JSON(objCartDealItemsLocal.product_sauce_type)
                        //                        dictPizza["psize"] = JSON(objCartDealItemsLocal.pizzaSize)
                        dictPizza["pcomnt"] = JSON(objCartDealItemsLocal.product_description)
                        dictPizza["variant"] = ""
                        
                        print("Customized \(objCartDealItemsLocal.isCustomized)")
                        if objCartDealItemsLocal.isCustomized == "1" {
                            dictPizza["prdvalue"] = JSON(getPriceFormatedValue(strPrice: setUpPriceForDeal(objCart: objCartDealItemsLocal)))
                        } else {
                            dictPizza["prdvalue"] = JSON(objCart.product_price)
                        }
                        arraySubData.append(dictPizza)
                        
                    })
                }
                dictPizza["dealname"] = JSON(arraySubData)
                arrayDealItems.append(dictPizza)
            }
            
        }
        
        //--Half half
        let arrayHalf = Array(globalRealm.objects(Cart.self)).filter { $0.product_type == "4"}
        if(arrayHalf.count > 0)
        {
            arrayHalf.forEach { (objCart) in
                
                var dictPizza = JSON()
                dictPizza["prdname"] = JSON(objCart.half_name)
                dictPizza["prdid"] = JSON(objCart.half_id)
                dictPizza["prdvalue"] = JSON(objCart.half_price)
                dictPizza["qty"] = "1"
                dictPizza["rmingr"] = ""
                
                var arraySubData : [JSON] = []
                
                let arrayDealsSubItems = Array(globalRealm.objects(CartDealItems.self)).filter { $0.cart_id == objCart.id}
                if(arrayDealsSubItems.count > 0)
                {
                    
                    arrayDealsSubItems.forEach({ (objCartDealItemsLocal) in
                        
                        var dictPizza = JSON()
                        dictPizza["prdname"] = JSON(objCartDealItemsLocal.product_name)
                        dictPizza["prdid"] = JSON(objCartDealItemsLocal.product_id)
                        dictPizza["qty"] = "1"
                        dictPizza["prdvalue"] = JSON(objCartDealItemsLocal.product_price)
                        dictPizza["prdingrd"] = JSON(objCartDealItemsLocal.ingr_name)
                        dictPizza["removetop"] = JSON(objCartDealItemsLocal.remove_ingr_name)
                        
                        dictPizza["pcomnt"] = JSON(objCartDealItemsLocal.product_description)
                        dictPizza["variant"] = ""
                        if objCartDealItemsLocal.isCustomized == "1"
                        {
                            dictPizza["prdvalue"] = JSON(getPriceFormatedValue(strPrice: setUpPriceForDeal(objCart: objCartDealItemsLocal)))
                        } else {
                            dictPizza["prdvalue"] = JSON(objCartDealItemsLocal.product_price)
                        }
                        arraySubData.append(dictPizza)
                        
                    })
                }
                dictPizza["halfandhalf"] = JSON(arraySubData)
                arrayHalfHalfItems.append(dictPizza)
            }
            
        }
        
        dictCheckoutJson["full"] = JSON(arrayFullPizzaItems)
        dictCheckoutJson["deal"] = JSON(arrayDealItems)
        dictCheckoutJson["half"] = JSON(arrayHalfHalfItems)
        
        print("checkout dictCheckoutJson - ",dictCheckoutJson)
        
        applyCouponCode(json: dictCheckoutJson)
//        CheckOutService(json : dictCheckoutJson)
    }
    
    func setUpPrice(objCart:Cart) -> Float
    {
        var price : Float = 0.0
        
        if(objCart.isTypePasta == "1")
        {
            price = Float(objCart.half_price) ?? 0.0
            if(!objCart.addedTopingsPrice.isEmpty)
            {
                price = price + Float(objCart.addedTopingsPrice)!
            }
            return price
        }
        
        let id = objCart.defaultTypeId
        let arrayPizzaType = setUpPizzaType()
        if(arrayPizzaType.contains { (json) -> Bool in
            if json["typeId"].stringValue == id
            {
                return true
            }
            return false
        })
        {
            switch id
            {
            case "0":
                price = Float(objCart.half_price) ?? 0.0
            case "1":
                price = Float(objCart.product_price) ?? 0.0
            case "2":
                price = Float(objCart.product_small_price) ?? 0.0
            default:
                price =  Float(objCart.product_price) ?? 0.0
            }
            
            if(!objCart.addedTopingsPrice.isEmpty)
            {
                price = price + Float(objCart.addedTopingsPrice)!
            }
            
            return price
            
        }
        return price
    }
    
    @objc func setUpPriceForDeal(objCart:CartDealItems) -> Float
    {
        var price : Float = 0.0
        
        let id = objCart.defaultTypeId
        let arrayPizzaType = setUpPizzaType()
        if(arrayPizzaType.contains { (json) -> Bool in
            if json["typeId"].stringValue == id
            {
                return true
            }
            return false
        })
        {
            switch id
            {
            case "0":
                price = Float(objCart.product_half_price) ?? 0.0
            case "1":
                price = Float(objCart.product_price) ?? 0.0
            case "2":
                price = Float(objCart.product_small_price) ?? 0.0
            default:
                price =  Float(objCart.product_price) ?? 0.0
            }
            
            if(!objCart.addedTopingsPrice.isEmpty)
            {
                price = price + Float(objCart.addedTopingsPrice)!
            }
            
            return price
            
        }
        return price
    }*/
}


//MARK:- Action Zone

extension CheckOutVC
{
    @IBAction func btnApplyAction(_ sender:UIButton)
    {
        if dictDiscount.count == 0{
            objOrder.strApplyCouponCode = self.txtCoupon.text ?? ""
            if objOrder.isCheckValidForApplyCouponCode() {
                self.txtCoupon.resignFirstResponder()
                let dict = GetCheckOutCartItem()
                applyCouponCode(json: dict)
            } else {
                makeToast(message: objOrder.strValidationMessage)
            }
        } else {
            self.txtCoupon.text = ""
            vwDiscount.isHidden = true
            dictDiscount = JSON()
            [self.txtCoupon].forEach({ (txtField) in
                txtField?.alpha = 1
                txtField?.isUserInteractionEnabled = true
            })
            [self.btnApplyOutlet].forEach({ (btn) in
                btn?.alpha = 1
                btn?.isUserInteractionEnabled = true
                btn?.setTitle(getCommonString(key: "Apply_key"), for: .normal)
            })
            SetTotalPrice()
        }
    }
    
    @IBAction func btnCheckOutAction(_ sender:UIButton)
    {
        if arrayCart.count == 0{
            makeToast(message: getValidationString(key: "Your_cart_is_empty_key"))
            return
        }
        let obj = objStoryboard.instantiateViewController(withIdentifier: "ConfirmOrderVC") as! ConfirmOrderVC
        obj.dictDiscountAmt = dictDiscount
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnDeleteAction(_ sender:UIButton)
    {
        print("sender \(sender.tag)")
        
        let alertController = UIAlertController(title: getCommonString(key: "Pizzaapp_key"), message: getCommonString(key: "Are_you_sure_you_want_to_remove_item_key"), preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            self.RemoveCartItem(index:sender.tag)
        }
        let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func btnPlusAction(_ sender:UIButton)
    {
        
    }
    
    @IBAction func btnMinusAction(_ sender:UIButton)
    {
        print("sender \(sender.tag)")
    }
    
}

//MARK:- Service
extension CheckOutVC
{
    func applyCouponCode(json : JSON)
    {
        print("Final order json - ",json)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kCheckCouponCode)"
            
            print("URL: \(url)")
            
            let orderUser =  Array(globalRealm.objects(CheckoutOrder.self))
            let objCheckOutOrder = orderUser[0]
            
            
            let param:[String:String] =  ["ordertype":"\(objCheckOutOrder.strSelectedOrderType)",
                                         "prod_info":json.rawString() ?? "",
                                         "amount":getPriceFormatedValue(strPrice: CartTotalPrice()),
                                         "store_id":objCheckOutOrder.strSelectedStoreId,
                                         "voucher_code":self.txtCoupon.text ?? ""
               
            ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.dictDiscount = json["data"]
                        [self.txtCoupon].forEach({ (txtField) in
                            txtField?.alpha = 0.5
                            txtField?.isUserInteractionEnabled = false
                        })
                        [self.btnApplyOutlet].forEach({ (btn) in
                            btn?.alpha = 1
                            btn?.isUserInteractionEnabled = true
                            btn?.setTitle(getCommonString(key: "Clear_key").uppercased(), for: .normal)
                        })
                        self.setupPriceAfterApplyCouponCode(dict: self.dictDiscount)
//                         makeToast(message: json["msg"].stringValue)
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
        
    }
    
}


