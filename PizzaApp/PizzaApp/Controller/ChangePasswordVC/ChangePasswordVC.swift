//
//  ChangePasswordVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 25/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class ChangePasswordVC: UIViewController {
    
    //MARK: - Outlet
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblOldPassword: UILabel!
    @IBOutlet weak var lblNewPassword: UILabel!
    @IBOutlet weak var lblConfirmPassword: UILabel!
    
    @IBOutlet weak var txtOldPassword: UITextField!
    @IBOutlet weak var txtNewPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    @IBOutlet weak var btnChangePasswordOutlet: CustomButton!
    
    //MARK: - Variable
    
    
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Change_password_key").capitalized, type: .back, barType: .white)
    }
    
    
    func setupUI()
    {
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 20, fontname: .semibold)
            lbl?.textColor = UIColor.appThemeRedColor
        }
        
        lblTitle.text = getCommonString(key: "Enter_old_password_details_key")
        
        [txtOldPassword,txtConfirmPassword,txtNewPassword].forEach { (txt) in
            txt?.font = themeFont(size: 17, fontname: .regular)
            txt?.delegate = self
            txt?.placeholder = getCommonString(key: "Enter_here_key")
            txt?.textColor = .black
        }
        
        [lblOldPassword,lblConfirmPassword,lblNewPassword].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = .black
        }
        
        lblOldPassword.text = getCommonString(key: "Old_password_key").uppercased()
        lblConfirmPassword.text = getCommonString(key: "New_password_key")
        lblNewPassword.text = getCommonString(key: "Confirm_password_key")
        
        btnChangePasswordOutlet.setUpThemeButtonUI()
        btnChangePasswordOutlet.setTitle(getCommonString(key: "Change_password_key").capitalized, for: .normal)
        btnChangePasswordOutlet.setTitleColor(UIColor.white, for: .normal)
        
    }
    
    
}

//MARK: - IBAction

extension ChangePasswordVC
{
    
    @IBAction func btnSaveTapped(_ sender: UIButton)
    {
        objUser = User()
        objUser.strOldPassword = self.txtOldPassword.text ?? ""
        objUser.strNewPassword = self.txtNewPassword.text ?? ""
        objUser.strConfirmPassword = self.txtConfirmPassword.text ?? ""
        
        if objUser.isChangePassword()
        {
            changePasswordAPI()
        }
        else
        {
            makeToast(message: objUser.strValidationMessage)
        }
    }
    
}

 //MARK:- API calling

extension ChangePasswordVC: UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

 //MARK:- API calling

extension ChangePasswordVC
{
    
    func changePasswordAPI()
    {
        self.view.endEditing(true)
        
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kChangePassword)"
            
            print("URL: \(url)")
            
            let param = ["lang" :strLang,
                         "id" : getUserDetail("id"),
                         "access_token" : getUserDetail("access_token"),
                         "current_password" : txtOldPassword.text ?? "",
                         "new_password" : txtNewPassword.text ?? ""]
            
            print("param :\(param)")
            
            self.showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    makeToast(message: json["msg"].stringValue)
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.logoutAPICalling()
                    }
                    else if json["flag"].stringValue == strAccessDenid
                    {
                        self.logoutAPICalling()
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
        
    }
    
}

