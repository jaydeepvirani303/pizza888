//
//  CustomizeVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 22/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

protocol delegateToAddCart {
    func setDelegateToAddCart(dict:JSON)
}


class CustomizeVC: UIViewController {
    
    //MARK:- Varibalde Declaration
    
    var typeDD = DropDown()
    var addCartDelegate : delegateToAddCart?
    var dictData = JSON()
    var dictDealData = JSON()

    var arrToppings:[JSON] = []
    var strMsg = String()
    var selectedCustomizedType =  selectCategoryItem.regular
    var handledCustomize:(JSON) -> Void = {dict in}
    var basePrice:Float = 0.0
    var sizePrice: Float = 0.0
    var saucePrice: Float = 0.0
    var arr_subType:[JSON] = []
    var isAlreayAddedHalf = false
    var dictAlreadyAddedHalf = JSON()
    
    //MARK:- Outlet zone
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblType: UILabel!
    @IBOutlet weak var lblItemDescription: UILabel!
    @IBOutlet weak var lblSubType: UILabel!
    @IBOutlet weak var lblInnerType: UILabel!
    @IBOutlet weak var txtViewDesciption: CustomTextview!
    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var lblTblHeaderTitle: UILabel!
    @IBOutlet weak var tblAddons: UITableView!
    @IBOutlet weak var lblTotalPrice: UILabel!
    @IBOutlet weak var heightOftblAddOns: NSLayoutConstraint!
    @IBOutlet weak var viewOfBottom: UIView!
    @IBOutlet weak var viewOfBottomNavigation: UIView!
    @IBOutlet weak var btnAddItemOutlet: UIButton!
    @IBOutlet weak var viewInnerType: UIView!
    
    @IBOutlet weak var viewSelectType: UIView!
    @IBOutlet weak var constraintHeightViewSelectType: NSLayoutConstraint!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var vwType: UIView!
    @IBOutlet weak var vwSubType: UIView!
    @IBOutlet weak var heightOfVariantView: NSLayoutConstraint!
    @IBOutlet weak var collectionOfVarient: UICollectionView!
    @IBOutlet weak var lblQuantity: UILabel!
    @IBOutlet weak var heightOfQuantity: NSLayoutConstraint!
    @IBOutlet weak var vwQuantity: UIView!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
//        viewDidLayoutSubviews()
        
        if dictData["type"].stringValue != "1"{
            getProductCustomize()
        } else {
            self.tblAddons.isHidden = true
        }
        
        print("dictData \(dictData)")
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblAddons.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        collectionOfVarient.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Customize_key"), type: .backWithCart, barType: .white)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblAddons.removeObserver(self, forKeyPath: "contentSize")
        
        collectionOfVarient.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        viewDidLayoutSubviews()       
        
    }
    override func viewDidLayoutSubviews() {
        
        viewOfBottom.roundCorners([.topRight,.bottomRight], radius: viewOfBottom.frame.size.height/2)
        
        viewOfBottomNavigation.layer.cornerRadius = viewOfBottomNavigation.frame.size.height/2
        viewOfBottomNavigation.layer.masksToBounds = true
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
//            print("contentSize:= \(tblAddons.contentSize.height)")
            self.heightOftblAddOns.constant = tblAddons.contentSize.height
        }
        
        if object is UICollectionView{
            self.heightOfVariantView.constant = collectionOfVarient.contentSize.height
            print("collectionview height \(collectionOfVarient.contentSize.height)")
        }
    }

}

//MARK:- setup UI

extension CustomizeVC
{
    func setupUI() {
        [lblItemName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .semibold)
        }
        
        [lblPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeRedColor
            lbl?.font = themeFont(size: 22, fontname: .medium)
        }
        
        [lblItemDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 18, fontname: .regular)
        }
        
        [lblType,lblSubType,lblInnerType].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 15, fontname: .regular)
        }
        
        [lblPlaceholder].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.text = getCommonString(key: "Enter_description_here_key")
        }
        
        [txtViewDesciption].forEach { (txtView) in
            txtView?.font = themeFont(size: 16, fontname: .regular)
            txtView?.textColor = .black
            txtView?.delegate = self
            txtView?.textContainerInset = UIEdgeInsets(top: 5, left: 8, bottom: 8, right: 8)
        }
        
        [lblTblHeaderTitle].forEach { (lbl) in
            lbl?.textColor = UIColor.white
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.text = getCommonString(key: "Toppings_key")
        }
        
        [lblTotalPrice].forEach { (lbl) in
            lbl?.font = themeFont(size: 22, fontname: .semibold)
            lbl?.textColor = UIColor.white
        }
        
        [btnAddItemOutlet].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 22, fontname: .semibold)
            btn?.setTitleColor(.white, for: .normal)
            btn?.setTitle(getCommonString(key: "Add_item_key").capitalized, for: .normal)
        }
        
        [lblQuantity].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 12, fontname: .regular)
        }
        
        lblItemName.text = dictData["product_name"].stringValue
        lblItemDescription.text = dictData["specification"].stringValue
        setUpPriceValue()
        
        [lblType,lblSubType,lblInnerType].forEach { (lbl) in
            lbl?.text = getCommonString(key: "Select_key")
        }
        
        arr_subType = dictData["sub_type"].arrayValue
        
        /*for i in 0..<arr_subType.count{
            var dict =  arr_subType[i]
            dict["selected_type"].stringValue = getCommonString(key: "Select_key")
            dict["selected_price"].stringValue = "0"
            arr_subType[i] = dict
        }*/
        
        if isAlreayAddedHalf {
            print("dictAlreadyAddedHalf \(dictAlreadyAddedHalf)")
            for i in 0..<arr_subType.count{
                var arr = self.arr_subType[i]["data"].arrayValue
                for  j in 0..<arr.count {
                    var dict = arr[j]
                    var arrAddedHalf = [String]()
                    if dictAlreadyAddedHalf["Half1"].exists() {
                        arrAddedHalf = dictAlreadyAddedHalf["Half1"]["addedVariant"].stringValue.components(separatedBy: ",")
                    } else {
                        arrAddedHalf = dictAlreadyAddedHalf["Half2"]["addedVariant"].stringValue.components(separatedBy: ",")
                    }
                    
                    for k in 0..<arrAddedHalf.count {
                        if dict["crtName"].stringValue == arrAddedHalf[k] {
                            dict["is_selected"] = "1"
                            self.arr_subType[i]["selected_price"].stringValue = dict["value"].stringValue
                            self.arr_subType[i]["type"].stringValue = dict["crtName"].stringValue
                        }
                    }
                    arr[j] = dict
                }
                self.arr_subType[i]["data"] = JSON(arr)
            }
        } else {
            for i in 0..<arr_subType.count{
                var arr = self.arr_subType[i]["data"].arrayValue
                for  j in 0..<arr.count {
                    var dict = arr[j]
                    if j == 0 {
                        dict["is_selected"] = "1"
                        self.arr_subType[i]["selected_price"].stringValue = dict["value"].stringValue
                        self.arr_subType[i]["type"].stringValue = dict["crtName"].stringValue
                    } else {
                        dict["is_selected"] = "0"
                    }
                    arr[j] = dict
                }
                self.arr_subType[i]["data"] = JSON(arr)
            }
        }
        
        
        CalculateTotalPrice()
        
        self.lblQuantity.text = "1"
        
        self.collectionOfVarient.reloadData()
        /*let arr_SubType = self.getArray(type: subType.base.rawValue)
        if arr_SubType.count > 0 {
            self.lblType.text = arr_SubType[0]["crtName"].stringValue
            dictData["baseType"].stringValue = arr_SubType[0]["crtName"].stringValue
        }
        
        if(arrSauce.count > 0)
        {
            self.lblSubType.text = arrSauce[0]
            dictData["sauce_type"].stringValue = arrSauce[0]
        }*/
        
        /*if(arrSubtype.count > 0)
        {
            self.lblType.text = arrSubtype[0]
            dictData["baseType"].stringValue = arrSubtype[0]
        }*/
        
       // let dict = arr_subType[0]
        
        
        if selectedCustomizedType == .deal || selectedCustomizedType == .half
        {
            viewInnerType.isHidden = true
            lblPrice.isHidden = true
            vwQuantity.isHidden = true
            heightOfQuantity.constant = 0
        }
       
        imgProduct.layer.cornerRadius = 5
        imgProduct.layer.masksToBounds = true
        
        imgProduct.sd_setShowActivityIndicatorView(true)
        imgProduct.sd_setIndicatorStyle(.gray)
        imgProduct.sd_setImage(with: URL(string: dictData["image"].stringValue) , placeholderImage:UIImage(named:"ic_placeholder"), options: .lowPriority, completed: nil)
        
        /*if(dictData["isTypePasta"].stringValue == "1")
        {
            viewSelectType.isHidden = true
            constraintHeightViewSelectType.constant = 0
        }*/
        
    }
    func setUpPriceValue()
    {
        //lblPrice.text = "\(strCurrenrcy) \(dictData["price"].stringValue)"
        //TODO:- set price according selection
        
        let id = dictData["defaultTypeId"].stringValue
        var addedPrice : Float = 0.0
        
       /* switch id
        {
            case "0":
                addedPrice = dictData["half_price"].floatValue
            case "1":
                addedPrice = dictData["price"].floatValue
            case "2":
                addedPrice = dictData["small_price"].floatValue
            default:
                addedPrice = dictData["price"].floatValue
        }*/
        addedPrice = dictData["product_price"].floatValue
        self.lblPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"
        self.arrToppings.forEach { (json) in
            let toppingPrice = json["ingredient_price"].floatValue
            
            if(json["is_selected"].stringValue == "1")
            {
                addedPrice = addedPrice + toppingPrice
            }
        }
        
        lblPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"
        
        if dictData["type"].stringValue == "1" {
            lblTotalPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"
        }
        
    }
  
}

//MARK:- Textview Delegate

extension CustomizeVC:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else
        {
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}


//MARK:- Tableview Delegate & Datasource

extension CustomizeVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrToppings.count == 0
        {
            let lbl = UILabel()
            lbl.text = strMsg
            lbl.font = themeFont(size: 20, fontname: .medium)
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrToppings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddOnsCell") as! AddOnsCell
        let dict  = arrToppings[indexPath.row]
        cell.setAddOnData(dict: dict)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var dict = self.arrToppings[indexPath.row]
        if dict["is_selected"].stringValue == "0"
        {
            dict["is_selected"].stringValue = "1"
        }
        else
        {
            dict["is_selected"].stringValue = "0"
        }
        self.arrToppings[indexPath.row] = dict
        self.tblAddons.reloadRows(at: [indexPath], with: .none)
        self.CalculateTotalPrice()
    }
}

//MARK:- Collection Method

extension CustomizeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return arr_subType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "VariantCell", for:indexPath) as! VariantCell
        let dict = arr_subType[indexPath.row]
        cell.lblTitle.text = dict["typName"].stringValue
        if dict["selected_type"].stringValue == "" {
            if let dict = arr_subType[indexPath.row]["data"].arrayValue.first(where: {$0["is_selected"].stringValue == "1"}) {
                cell.lblTypeName.text = "\(dict["crtName"].stringValue) (+\(dict["value"].stringValue))"
            }
        } else {
            cell.lblTypeName.text = dict["selected_type"].stringValue
        }
        cell.btnSelectVariantOutlet.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.size.width - 20 )/2, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
    
}
//MARK:- Other methods
extension CustomizeVC
{
    func CalculateTotalPrice()  //For deal
    {
        
        if(selectedCustomizedType == .deal)
        {
            print("Customize deal data - ",dictDealData)
            let originalPrice = dictDealData["dealPrice"].floatValue
            var addedPrice = originalPrice
            
//            addedPrice = 0.0
            self.arrToppings.forEach { (json) in
                let toppingPrice = json["ingredient_price"].floatValue
                
                if(json["is_selected"].stringValue == "1" && json["is_default"].stringValue != "1")
                {
                    addedPrice = addedPrice + toppingPrice
                }
            }
            for i in 0..<arr_subType.count{
                addedPrice += arr_subType[i]["selected_price"].floatValue
            }
            lblTotalPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"
        }else if(selectedCustomizedType == .regular)
        {
//            setUpPrice()
//            self.dictData["defaultTypeId"].stringValue =  dict["typeId"].stringValue
            var originalPrice = self.getPrice(dict: self.dictData)
            var addedPrice = originalPrice
            
            self.arrToppings.forEach { (json) in
                let toppingPrice = json["ingredient_price"].floatValue
                
                if(json["is_selected"].stringValue == "1" && json["is_default"].stringValue != "1")
                {
                    addedPrice = addedPrice + toppingPrice
                }
            }
//            addedPrice = addedPrice + self.basePrice + self.saucePrice + self.sizePrice
            for i in 0..<arr_subType.count{
                addedPrice += arr_subType[i]["selected_price"].floatValue
            }
            let strQuntity = self.lblQuantity.text ?? "1"
            addedPrice = addedPrice * Float(strQuntity)!
            self.lblTotalPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"
        } else if selectedCustomizedType == .half {
            
            let originalPrice = dictData["product_half_price"].floatValue
            var addedPrice = originalPrice
            
            //            addedPrice = 0.0
            self.arrToppings.forEach { (json) in
                let toppingPrice = json["ingredient_price"].floatValue
                
                if(json["is_selected"].stringValue == "1" && json["is_default"].stringValue != "1")
                {
                    addedPrice = addedPrice + toppingPrice
                }
            }
            for i in 0..<arr_subType.count{
                addedPrice += arr_subType[i]["selected_price"].floatValue
            }
            lblTotalPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"
        }
        
    }
    func
        setUpPrice()
    {
        var addedPrice : Float = 0.0

        let dict = dictData
        
        if(dict["isTypePasta"].stringValue == "1")
        {
            addedPrice = dict["half_price"].floatValue
            self.lblPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"
            self.arrToppings.forEach { (json) in
                let toppingPrice = json["ingredient_price"].floatValue
                
                if(json["is_selected"].stringValue == "1" && json["is_default"].stringValue != "1")
                {
                    addedPrice = addedPrice + toppingPrice
                }
            }
            
            lblTotalPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"
            
            return
        }
        
        /*let id = dict["defaultTypeId"].stringValue
//        let arrayPizzaType = setUpPizzaType()
        let arrayPizzaType = getArray(type: subType.size.rawValue)
        if(arrayPizzaType.contains { (json) -> Bool in
            if json["typeId"].stringValue == id
            {
                lblInnerType.text = json["typeName"].stringValue
                dictData["pizzaSize"].stringValue = json["typeName"].stringValue
                return true
            }
            return false
        })
        {
           
            switch id
            {
                case "0":
                    addedPrice = dict["half_price"].floatValue
                case "1":
                    addedPrice = dict["price"].floatValue
                case "2":
                    addedPrice = dict["small_price"].floatValue
                default:
                    addedPrice = dict["price"].floatValue
            }
        
            self.lblPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"
            self.arrToppings.forEach { (json) in
                let toppingPrice = json["ingredient_price"].floatValue
                
                if(json["is_selected"].stringValue == "1" && json["is_default"].stringValue != "1")
                {
                    addedPrice = addedPrice + toppingPrice
                }
            }
            
            lblTotalPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"

            
        }*/
    
        addedPrice = dict["product_price"].floatValue
        self.lblPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"
        self.arrToppings.forEach { (json) in
            let toppingPrice = json["ingredient_price"].floatValue
            
            if(json["is_selected"].stringValue == "1" && json["is_default"].stringValue != "1")
            {
                addedPrice = addedPrice + toppingPrice
            }
        }
        
        lblTotalPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", addedPrice))"
    }

}
//MARK:- Setup DropDown

extension CustomizeVC
{
    func setUpDropDown(tag:selectPizzaType)
    {
        if tag == .suace
        {
            typeDD.dataSource = getStringArray(type: subType.sauce.rawValue)
            typeDD.selectionAction = { (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.lblSubType.text = item
                self.dictData["sauce_type"].stringValue = item
                let arrSauceType = self.getArray(type: subType.sauce.rawValue)
                let dict  = arrSauceType[index]
                print("dict price \(dict)")
                self.dictData["defaultTypeId"].stringValue =  dict["typeId"].stringValue
                self.saucePrice = dict["value"].floatValue
                /*var price = self.getPrice(dict: self.dictData)
                self.saucePrice = dict["value"].floatValue
                price = price + self.basePrice + self.saucePrice + self.sizePrice
                self.lblTotalPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", price))"*/
                self.CalculateTotalPrice()
                self.typeDD.hide()
            }
        }
        else if tag == .type
        {
            typeDD.dataSource = getStringArray(type: subType.size.rawValue)
            typeDD.selectionAction = { (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.lblInnerType.text = item
                self.dictData["pizzaSize"].stringValue = item
                let arrInnerType = self.getArray(type: subType.size.rawValue)
                let dict  = arrInnerType[index]
                print("dict price \(dict)")
                self.dictData["defaultTypeId"].stringValue =  dict["typeId"].stringValue
                self.sizePrice = dict["value"].floatValue
                /*var price = self.getPrice(dict: self.dictData)
                self.sizePrice = dict["value"].floatValue
                price = price + self.basePrice + self.saucePrice + self.sizePrice
                self.lblTotalPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", price))"*/
                self.CalculateTotalPrice()
                self.typeDD.hide()
            }
            /*typeDD.dataSource = getPizzaTypeNames()
            typeDD.selectionAction = { (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.typeDD.hide()
                
                var id = ""
                if(setUpPizzaType().contains { (json) -> Bool in
                    if json["typeName"].stringValue == item
                    {
                        id = json["typeId"].stringValue
                        self.dictData["pizzaSize"].stringValue = item

                        return true
                    }
                    return false
                })
                {
                    
                }
                
                self.dictData["defaultTypeId"].stringValue =  id
                
                self.setUpPrice()
            }*/
        }
        else if tag == .subType
        {
            /*typeDD.dataSource = arrSubtype
            typeDD.selectionAction = { (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.lblType.text = item
                self.dictData["baseType"].stringValue = item
                self.typeDD.hide()
            }*/
            typeDD.dataSource = getStringArray(type: subType.base.rawValue)
            typeDD.selectionAction = { (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.lblType.text = item
                self.dictData["baseType"].stringValue = item
                let arrSubType = self.getArray(type: subType.base.rawValue)
                let dict  = arrSubType[index]
                print("dict price \(dict)")
                self.dictData["defaultTypeId"].stringValue =  dict["typeId"].stringValue
                self.basePrice = dict["value"].floatValue
                /*var price = self.getPrice(dict: self.dictData)
                self.basePrice = dict["value"].floatValue
                price = price + self.basePrice + self.saucePrice + self.sizePrice
                self.lblTotalPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", price))"*/
                self.CalculateTotalPrice()
                self.typeDD.hide()
            }
        }
    }
    
    func getStringArray(type:String) -> [String]{
        var arrStrBase:[String] = []
        var dictType = JSON()
        let arr_subType = dictData["sub_type"].arrayValue
        
        if arr_subType.contains(where: { (dictSubType) -> Bool in
            if dictSubType["typName"].stringValue == type{
                dictType = dictSubType
                return true
            }
            return false
        }) {
            let arrBase = dictType[type].arrayValue
            arrStrBase = arrBase.map({$0["crtName"].stringValue})
        }
        return arrStrBase
    }
    
    func getArray(type:String) -> [JSON]{
        var arrType:[JSON] = []
        var dictType = JSON()
        let arr_subType = dictData["sub_type"].arrayValue
        
        if arr_subType.contains(where: { (dictSubType) -> Bool in
            if dictSubType["typName"].stringValue == type{
                dictType = dictSubType
                return true
            }
            return false
        }) {
            arrType = dictType[type].arrayValue
        }
        return arrType
    }
    
}


//MARK:- Action Zone

extension CustomizeVC
{
    @IBAction func btnAddItemAction(_ sender:UIButton)
    {
        var strSelectedToppingId = [String]()
        var strSelectedTopping = [String]()
        
        var strDefaultToppingRemoveId = [String]()
        var strDefaultToppingRemoveName = [String]()
        
        var addedTopingsPrice : Float = 0.0
        
        print("arrToppings - ",arrToppings)
        
        
        for i in 0..<arrToppings.count
        {
            let dict =  arrToppings[i]
            if dict["is_selected"].stringValue == "1" &&  dict["is_default"].stringValue != "1"
            {
                strSelectedToppingId.append(dict["ingr_id"].stringValue)
                strSelectedTopping.append(dict["ingredient_name"].stringValue)
                
                addedTopingsPrice = addedTopingsPrice + dict["ingredient_price"].floatValue
                
            }
            else{
                if(dict["is_default"].stringValue == "1" && dict["is_selected"].stringValue == "0")
                {
                    strDefaultToppingRemoveId.append(dict["ingr_id"].stringValue)
                    strDefaultToppingRemoveName.append(dict["ingredient_name"].stringValue)
                }
                
            }
        }
        
        var strVariant = [String]()
        var price = self.getPrice(dict: self.dictData)
        price += addedTopingsPrice
//        price = price + self.basePrice + self.saucePrice + self.sizePrice + addedTopingsPrice
        for i in 0..<arr_subType.count{
            price += arr_subType[i]["selected_price"].floatValue
            if arr_subType[i]["selected_type"].stringValue != getCommonString(key: "Select_key"){
                strVariant.append(arr_subType[i]["type"].stringValue)
            }
        }
        
        dictData["ingr_add"].stringValue = strSelectedToppingId.joined(separator: ",")
        dictData["ingr_name"].stringValue = strSelectedTopping.joined(separator: ",")
        dictData["remove_ingr_id"].stringValue = strDefaultToppingRemoveId.joined(separator: ",")
        dictData["remove_ingr_name"].stringValue = strDefaultToppingRemoveName.joined(separator: ",")
        dictData["addedTopingsPrice"].stringValue = "\(addedTopingsPrice)"
        dictData["description"].stringValue = self.txtViewDesciption.text ?? ""
        dictData["customized"].stringValue = "1"
        if selectedCustomizedType == .deal ||  selectedCustomizedType == .half{
            price = 0.0
            price += addedTopingsPrice
            strVariant = [String]()
            for i in 0..<arr_subType.count{
                price += arr_subType[i]["selected_price"].floatValue
                if arr_subType[i]["selected_type"].stringValue != getCommonString(key: "Select_key"){
                    strVariant.append(arr_subType[i]["type"].stringValue)
                }
            }
            
            dictData["finalPrice"].stringValue = getPriceFormatedValue(strPrice: price)
        } else {
            dictData["finalPrice"].stringValue = getPriceFormatedValue(strPrice: price)
        }
        
        dictData["addedVariant"].stringValue = strVariant.count != 0 ? strVariant.joined(separator: ",") : ""
        dictData["quantity"].stringValue = self.lblQuantity.text ?? "1"
//        //TODO:- change customize check
//        if(dictData["type"].stringValue == "3")
//        {
//            dictData["product_name"].stringValue = dictData["name"].stringValue
//
//        }
        
        print("Customized dict - ",dictData)
        
        //TODO:- Uncomment below
        if selectedCustomizedType == .regular{
        
            self.AddToCart(dict: dictData)
            self.addCartDelegate?.setDelegateToAddCart(dict: dictData)
        }
        else if selectedCustomizedType == .half
            
        {
            dictData["is_added"] = "1"
            handledCustomize(dictData)
        }
        else
        {
            dictData["is_added"] = "1"
            handledCustomize(dictData)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubTypeAction(_ sender:UIButton)
    {
        configureDropdown(dropdown: typeDD, sender: sender)
        setUpDropDown(tag: .subType)
        typeDD.show()
    }
    
    @IBAction func btnSelectSauceAction(_ sender:UIButton)
    {
        configureDropdown(dropdown: typeDD, sender: sender)
        setUpDropDown(tag: .suace)
        typeDD.show()
    }
    
    @IBAction func btnSelectTypeAction(_ sender:UIButton)
    {
        configureDropdown(dropdown: typeDD, sender: sender)
        setUpDropDown(tag: .type)
        typeDD.show()
    }
    
    @IBAction func btnPlusAction(_ sender:UIButton){
        let strQuntity = self.lblQuantity.text ?? "1"
        self.lblQuantity.text = "\(Int(strQuntity)! + 1)"
        self.CalculateTotalPrice()
    }
    
    @IBAction func btnMinusAction(_ sender:UIButton){
        let strQuntity = self.lblQuantity.text ?? "1"
        if strQuntity == "1" {
            return
        }
        self.lblQuantity.text = "\(Int(strQuntity)! - 1)"
        self.CalculateTotalPrice()
    }
    
    @IBAction func btnSelectVariantAction(_ sender:UIButton){
        if selectedCustomizedType == .half {
            if isAlreayAddedHalf {
                return
            }
        }
        let arrStrBase = arr_subType[sender.tag]["data"].arrayValue.map({"\($0["crtName"].stringValue) (+\($0["value"].stringValue))"})
        typeDD.dataSource = arrStrBase
        configureDropdown(dropdown: typeDD, sender: sender)
        typeDD.show()
        typeDD.selectionAction = { (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            var dict = self.arr_subType[sender.tag]
            var arrType = self.arr_subType[sender.tag]["data"].arrayValue
            for i in 0..<arrType.count {
                var dict = arrType[i]
                dict["is_selected"] = "0"
                arrType[i] = dict
            }
            dict["selected_type"].stringValue = item
            dict["type"].stringValue = arrType[index]["crtName"].stringValue
            arrType[index]["is_selected"] = "1"
            let price = arrType[index]["value"].stringValue
            dict["selected_price"].stringValue = price
            self.arr_subType[sender.tag]["data"] = JSON(arrType)
            self.arr_subType[sender.tag] = dict
            print("arr_subType \(self.arr_subType)")
            self.collectionOfVarient.reloadData()
            self.CalculateTotalPrice()
            self.typeDD.hide()
        }
    }
}

//MARK:- Service

extension CustomizeVC
{
    func getProductCustomize()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kProductCustomize)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
//                          "type":dictData["type"].stringValue,
                          "type":"2",
                          "store_id": dictStoreDetail["id"].stringValue,
                          "cst_id":dictData["product_id"].stringValue
            ]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrToppings = []
                        self.arrToppings = json["data"]["extra_topping_data"].arrayValue
                        for i in 0..<self.arrToppings.count
                        {
                            var dict = self.arrToppings[i]
                            dict["is_selected"].stringValue = "0"
                            dict["is_default"].stringValue = "0"
                            self.arrToppings[i] = dict
                        }
                        
                        let strTopping = self.dictData["ingr_add"].stringValue
                        let arrSelectedTopping = strTopping.components(separatedBy: ",")
                        
                        for j in 0..<arrSelectedTopping.count
                        {
                            let strSelectedTopping = arrSelectedTopping[j]
                            for k in 0..<self.arrToppings.count
                            {
                                var dict = self.arrToppings[k]
                                if dict["ingr_id"].stringValue == strSelectedTopping
                                {
                                    dict["is_selected"].stringValue = "1"
                                    dict["is_default"].stringValue = "1"
                                    self.arrToppings[k] = dict
                                }else{
                                    self.arrToppings[k] = dict

                                }
                            }
                        }
                        
                        print("After fill array topings - ",self.arrToppings)
                        
                        self.CalculateTotalPrice()
                    }
                    else
                    {
                        self.strMsg = json["msg"].stringValue
                        self.arrToppings = []
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblAddons.reloadData()
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}



