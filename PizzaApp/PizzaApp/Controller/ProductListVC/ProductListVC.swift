//
//  ProductListVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 11/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class ProductListVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrProductList:[JSON] = []
    var dictData = JSON()
    var handleAddToDeal:(_ dealData:JSON,_ index:Int) -> Void = {dict,tag in}
    var dictDealData = JSON()
    //MARK:- Outlet Zone
    
    @IBOutlet weak var tblProductList: UITableView!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
       
        arrProductList = dictData["items"].arrayValue
        self.tblProductList.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: dictData["name"].stringValue, type: .backWithCart, barType: .white)
    }

}

//MARK:- Tableview Deleagate and Datasoure

extension ProductListVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrProductList.count == 0
        {
            let lbl = UILabel()
            lbl.text = "No record found"
            lbl.font = themeFont(size: 20, fontname: .medium)
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.black
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return arrProductList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DealDetailCell") as! DealDetailCell
        let dict = arrProductList[indexPath.row]
        cell.lblItemName.text = dict["product_name"].stringValue
        cell.lblItemPrice.text = "\(strCurrenrcy) \(dict["product_price"].stringValue)"
        
        cell.imgProduct.sd_setShowActivityIndicatorView(true)
        cell.imgProduct.sd_setIndicatorStyle(.gray)
        cell.imgProduct.sd_setImage(with: URL(string: dict["image"].stringValue) , placeholderImage:UIImage(named:"ic_placeholder"), options: .lowPriority, completed: nil)
        
        
        if dict["type"].stringValue == "1"
        {
            cell.viewOfCustomize.isHidden = true
        }else if(dict["type"].stringValue == "2")
        {
            cell.viewOfCustomize.isHidden = false
        }
        else{
            cell.viewOfCustomize.isHidden = true
        }
        cell.btnAddDealOutlet.tag = indexPath.row
        cell.btnCustomizeOutlet.tag = indexPath.row
        return cell
    }
    
}

extension ProductListVC
{
    @IBAction func btnAddDealAction(_ sender:UIButton)
    {
        var dict = arrProductList[sender.tag]
        print("dict \(dict)")
        /*
         "product_id" : "107",
         "type" : "2",
         "price" : "14.40",
         "product_name" : "Chicken Supreme",
         "is_customize" : 1,
         "specification" : "Chicken, mushrooms, pineapple, capsicum, onion",
         "half_price" : "10.90",
         "small_price" : "8.90",
         "ingr_add" : "31,8,7,44,19,27"
 
        */
        
        //TODO:- change customize
        if(dict["type"].stringValue == "2")
        {
            /*dict["ingr_add"].stringValue = ""
            dict["ingr_name"].stringValue = ""
            dict["description"].stringValue = ""
            dict["remove_ingr_id"].stringValue = ""
            dict["remove_ingr_name"].stringValue = ""
            dict["addedTopingsPrice"].stringValue = ""
            dict["type"].stringValue = "2"
            */
            
            dict["customized"].stringValue = "0"
            handleAddToDeal(dict,2)
        }
        else if(dict["type"].stringValue == "1" || dict["type"].stringValue == "3")
        {
            dict["customized"].stringValue = "0"
            handleAddToDeal(dict,1)
        }
    
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCustomizeAction(_ sender:UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "CustomizeVC") as! CustomizeVC
        obj.dictData = arrProductList[sender.tag]
        obj.dictDealData = dictDealData

        obj.selectedCustomizedType = .deal
        obj.handledCustomize = {[weak self](dict) in
            /*var dictInner = self!.arrProductList[sender.tag]
            dictInner["name"].stringValue = dict["product_name"].stringValue
            dictInner["product_name"].stringValue = dict["product_name"].stringValue

            dictInner["ingr_add"].stringValue = dict["ingr_add"].stringValue
            dictInner["ingr_name"].stringValue = dict["ingr_name"].stringValue
            dictInner["description"].stringValue = dict["description"].stringValue
            dictInner["remove_ingr_id"].stringValue = dict["remove_ingr_id"].stringValue
            dictInner["remove_ingr_name"].stringValue = dict["remove_ingr_name"].stringValue
            dictInner["addedTopingsPrice"].stringValue = dict["addedTopingsPrice"].stringValue
            */
            self!.handleAddToDeal(dict,2)
            self?.navigationController?.popViewController(animated: true)
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

