//
//  MyProfileVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 25/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class MyProfileVC: UIViewController {
    
    //MARK: - Outlet
   
    @IBOutlet weak var btnEditProfile: CustomButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPhoneNUmber: UILabel!
    @IBOutlet weak var lblLocation: UILabel!    
    @IBOutlet weak var vwContent: UIView!

    //MARK: - Variable
    
    var dictData = JSON()
    
    
    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "My_profile_key"), type: .back, barType: .white)
    
        
    }
    
    
    func setData(dict:JSON)
    {
        vwContent.isHidden = false
        lblName.text = dict["username"].stringValue
        lblEmail.text = dict["email_id"].stringValue
        lblLocation.text = dict["address"].stringValue
        lblPhoneNUmber.text = dict["phone"].stringValue
    }
    
    func setupUI()
    {
        vwContent.isHidden = true
        btnEditProfile.setUpThemeButtonUI()
        btnEditProfile.setTitle(getCommonString(key: "Edit_Profile_key").capitalized, for: .normal)
        btnEditProfile.setTitleColor(UIColor.white, for: .normal)
        
        [lblName,lblEmail,lblLocation,lblPhoneNUmber].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 16, fontname: .regular)
        }
        
        getUser()
    }
    
    
    @IBAction func btnEditProfileTapped(_ sender: UIButton) {
        
        let obj = objStoryboard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        obj.selectedController  = .editProfile
        obj.delegateProfile = self
        obj.dictData = dictData
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    
}

//MARK:- Edit Profile Delegate

extension MyProfileVC : delegateEditProfile
{
    func editProfileDelegate() {
        getUser()
    }
    
    
}

//MARK:- Service


extension MyProfileVC
{
    func getUser()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kSaveUserProfile)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "id":getUserDetail("id"),
                          "access_token" : getUserDetail("access_token"),
                          "id_update": "0"
            ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.dictData = json["data"]
                        self.setData(dict: self.dictData)
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}

