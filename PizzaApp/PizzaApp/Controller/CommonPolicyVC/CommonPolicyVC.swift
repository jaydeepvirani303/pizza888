//
//  CommonPolicyVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 26/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON
import WebKit

class CommonPolicyVC: UIViewController {
    
    var selectedPolicyType =  selectPolicyType.TermsCondition
    var strTitle =  String()
    var dict = JSON()
    var selectedRedirectVC = selectPolicyRedirectVC.helpSetting
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtPolicy: UITextView!
    @IBOutlet weak var topOfView: NSLayoutConstraint!
    @IBOutlet weak var webView: WKWebView!
    
    //MARK:- VeiwLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        setupUI()
        GetPolicyService()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        //self.navigationController?.isNavigationBarHidden = false
        
        if selectedPolicyType == .AboutUs
        {
             setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: dict["name"].stringValue.capitalized, type: .sidemenu, barType: .white)
            
            
        }
        else
        {            
            setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: dict["name"].stringValue.capitalized, type: .back, barType: .white)
        }
        
        if selectedRedirectVC == .helpSetting
        {
            self.topOfView.constant = 20
        }
        
    }
    
}

//MARK:- Setup UI
extension CommonPolicyVC
{
    func setupUI()  {
        
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .semibold)
            lbl?.textColor = UIColor.appThemeRedColor
        }
        
        lblTitle.text = dict["name"].stringValue.capitalized
        
        [txtPolicy].forEach { (txtView) in
            txtView?.font = themeFont(size: 17, fontname: .regular)
            txtView?.textColor = UIColor.appThemeLightGrayColor
        }
        
        
       
        
    }
}
//MARK:- Other methods
extension CommonPolicyVC
{
    func SetPolicyData(dict:JSON)
    {
        if(selectedPolicyType == .DeliveryPolicy)
        {
            webView.loadHTMLString(dict["delivery_policy"]["description"].stringValue, baseURL: nil)
//            txtPolicy.attributedText = dict["delivery_policy"].stringValue.htmlToAttributedString
        }
        else if(selectedPolicyType == .AboutUs)
        {
            webView.loadHTMLString(dict["about_us"]["description"].stringValue, baseURL: nil)
//            txtPolicy.attributedText = dict["about_us"].stringValue.htmlToAttributedString
        }
        else if(selectedPolicyType == .RefundPolicy)
        {
            webView.loadHTMLString(dict["refund_policy"]["description"].stringValue, baseURL: nil)
//            txtPolicy.attributedText = dict["refund_policy"].stringValue.htmlToAttributedString
        }
        else if(selectedPolicyType == .TermsCondition)
        {
            webView.loadHTMLString(dict["term_condition"]["description"].stringValue, baseURL: nil)
//            txtPolicy.attributedText = dict["term_condition"].stringValue.htmlToAttributedString
        }
        else if(selectedPolicyType == .PrivacyPolicy)
        {
            webView.loadHTMLString(dict["privacy"]["description"].stringValue, baseURL: nil)
//            txtPolicy.attributedText = dict["privacy"].stringValue.htmlToAttributedString
        }
    }
}
//MARK:- Service
extension CommonPolicyVC
{
    
    func GetPolicyService()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
             let url = "\(kBasicURL)\(kGetPolicy)"
            
             print("URL: \(url)")
            
             let param =  ["lang":strLang
             
             ]
            
             print("Param : \(param)")
            
             showLoader()
            
             CommonService().Service(url: url, param: param) { (respones) in
             
             self.stopLoader()
             
                 if let json = respones.value
                 {
                     print("JSON : \(json)")
                
                     if json["flag"].stringValue == strSuccessResponse
                     {
                     
                        self.SetPolicyData(dict: json["data"])
                     }
                     else
                     {
                        makeToast(message: json["msg"].stringValue)
                     }
                
                 }
                 else
                 {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                 }
             }
         
         }
         else
         {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
         }
        
    }
    
}
