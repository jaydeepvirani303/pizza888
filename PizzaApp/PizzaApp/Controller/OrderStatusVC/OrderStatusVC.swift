//
//  OrderStatusVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 25/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class OrderStatusVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrOrderStatus:[JSON] = []
    var dictOrderTrack = JSON()
    
    //MARK:- Outlet Zone
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblOrderNumber: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var tblOrderStatus: UITableView!
    @IBOutlet weak var heightIOftblOrderStatus: NSLayoutConstraint!
    
    //MARK:- ViewLife cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        tblOrderStatus.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Order_status_key"), type: .back, barType: .white)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblOrderStatus.removeObserver(self, forKeyPath: "contentSize")
        
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblOrderStatus.contentSize.height)")
            self.heightIOftblOrderStatus.constant = tblOrderStatus.contentSize.height
        }
    }

    
}


//MARK:- Array Setup

extension OrderStatusVC
{
    func setupOrderStatusArray() -> [JSON]
    {
        var arrMenu:[JSON] = []
        
        var dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Order_received_key").uppercased()
        dict["is_selected"].stringValue = "0"
        dict["is_uppar_selected"].stringValue = "0"
        dict["is_lower_selected"].stringValue = "0"
        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Cooking_now_key").uppercased()
        dict["is_selected"].stringValue = "0"
        dict["is_uppar_selected"].stringValue = "0"
        dict["is_lower_selected"].stringValue = "0"
        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Order_packed_key").uppercased()
        dict["is_selected"].stringValue = "0"
        dict["is_uppar_selected"].stringValue = "0"
        dict["is_lower_selected"].stringValue = "0"
        arrMenu.append(dict)
        
        if dictOrderTrack["dtype"].stringValue == "1" {
            
            dict = JSON()
            dict["name"].stringValue = getCommonString(key: "On_board_key").uppercased()
            dict["is_selected"].stringValue = "0"
            dict["is_uppar_selected"].stringValue = "0"
            dict["is_lower_selected"].stringValue = "0"
            arrMenu.append(dict)
            
            dict = JSON()
            dict["name"].stringValue = getCommonString(key: "Picked_by_customer_key").uppercased()
            dict["is_selected"].stringValue = "0"
            dict["is_uppar_selected"].stringValue = "0"
            dict["is_lower_selected"].stringValue = "0"
            arrMenu.append(dict)
        } else {
            dict = JSON()
            dict["name"].stringValue = getCommonString(key: "Delivered_key").uppercased()
            dict["is_selected"].stringValue = "0"
            dict["is_uppar_selected"].stringValue = "0"
            dict["is_lower_selected"].stringValue = "0"
            arrMenu.append(dict)
        }
        
        return arrMenu
    }
}

//MARK:- Setup UI
extension OrderStatusVC
{
    func setupUI()
    {
        [lblOrderDate,lblOrderNumber].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .regular)
            lbl?.textColor = UIColor.black
        }
        
        lblOrderNumber.text = "#\(dictOrderTrack["order_num"].stringValue)"
        lblOrderDate.text = stringTodate(OrignalFormatter: dateFormatter.dateFormatter1.rawValue, YouWantFormatter: dateFormatter.dateFormatter2.rawValue, strDate: dictOrderTrack["order_dttime"].stringValue)
        
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .semibold)
            lbl?.textColor = UIColor.appThemeRedColor
        }
        
        lblTitle.text = getCommonString(key: "Order_process_key").uppercased()
        
        arrOrderStatus = setupOrderStatusArray()
       
        if dictOrderTrack["order_status"].stringValue == "0" || dictOrderTrack["order_status"].stringValue == "1" || dictOrderTrack["order_status"].stringValue == "2" {
            var dict = arrOrderStatus[0]
            dict["is_selected"].stringValue = "1"
            arrOrderStatus[0] = dict
        }
        
        if dictOrderTrack["order_status"].stringValue == "3"  {
            var dict = arrOrderStatus[0]
            dict["is_lower_selected"].stringValue = "1"
            dict["is_selected"].stringValue = "1"
            arrOrderStatus[0] = dict
            
            dict = arrOrderStatus[1]
            dict["is_selected"].stringValue = "1"
            dict["is_uppar_selected"].stringValue = "1"
            dict["is_lower_selected"].stringValue = "0"
            arrOrderStatus[1] = dict
        }
        
        if dictOrderTrack["order_status"].stringValue == "4"  {
            
            var dict = arrOrderStatus[0]
            dict["is_lower_selected"].stringValue = "1"
            dict["is_uppar_selected"].stringValue = "1"
            dict["is_selected"].stringValue = "1"
            arrOrderStatus[0] = dict
            
            dict = arrOrderStatus[1]
            dict["is_selected"].stringValue = "1"
            dict["is_uppar_selected"].stringValue = "1"
            dict["is_lower_selected"].stringValue = "1"
            arrOrderStatus[1] = dict
            
            dict = arrOrderStatus[2]
            dict["is_selected"].stringValue = "1"
            dict["is_uppar_selected"].stringValue = "1"
            dict["is_lower_selected"].stringValue = "0"
            arrOrderStatus[2] = dict
            
            if dictOrderTrack["dtype"].stringValue == "1" {
                dict = arrOrderStatus[2]
                dict["is_selected"].stringValue = "1"
                dict["is_uppar_selected"].stringValue = "1"
                dict["is_lower_selected"].stringValue = "1"
                arrOrderStatus[2] = dict
                
                var dict = arrOrderStatus[3]
                dict["is_selected"].stringValue = "1"
                dict["is_uppar_selected"].stringValue = "1"
                dict["is_lower_selected"].stringValue = "1"
                arrOrderStatus[3] = dict
            }
        }
        
        if dictOrderTrack["order_status"].stringValue == "5"  {
            
            var dict = arrOrderStatus[0]
            dict["is_lower_selected"].stringValue = "1"
            dict["is_uppar_selected"].stringValue = "1"
            dict["is_selected"].stringValue = "1"
            arrOrderStatus[0] = dict
            
            dict = arrOrderStatus[1]
            dict["is_selected"].stringValue = "1"
            dict["is_uppar_selected"].stringValue = "1"
            dict["is_lower_selected"].stringValue = "1"
            arrOrderStatus[1] = dict
            
            dict = arrOrderStatus[2]
            dict["is_selected"].stringValue = "1"
            dict["is_uppar_selected"].stringValue = "1"
            dict["is_lower_selected"].stringValue = "1"
            arrOrderStatus[2] = dict
            
            if dictOrderTrack["dtype"].stringValue == "2" {
                var dict = arrOrderStatus[3]
                dict["is_selected"].stringValue = "1"
                dict["is_uppar_selected"].stringValue = "1"
                dict["is_lower_selected"].stringValue = "1"
                arrOrderStatus[3] = dict
            } else {
                var dict = arrOrderStatus[3]
                dict["is_selected"].stringValue = "1"
                dict["is_uppar_selected"].stringValue = "1"
                dict["is_lower_selected"].stringValue = "1"
                arrOrderStatus[3] = dict
                
                dict = arrOrderStatus[4]
                dict["is_selected"].stringValue = "1"
                dict["is_uppar_selected"].stringValue = "1"
                dict["is_lower_selected"].stringValue = "1"
                arrOrderStatus[4] = dict
            }
        }
        
        print("Order Status \(arrOrderStatus)")
        
        self.tblOrderStatus.reloadData()
        self.tblOrderStatus.tableFooterView = UIView()
    }
}


//MARK:- Tableview Delegate & Datasource

extension OrderStatusVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrderStatus.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderStatusCell") as! OrderStatusCell
        let dict = arrOrderStatus[indexPath.row]
        if indexPath.row == 0
        {
            cell.vwUpper.isHidden = true
        }
        else
        {
            cell.vwUpper.isHidden = false
        }
        
        if indexPath.row == arrOrderStatus.count - 1
        {
             cell.vcLower.isHidden = true
        }
        
        cell.setData(dict)
        return cell
    }
    
    
}
