//
//  DeliveryDetailVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 21/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class DeliveryDetailVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dict = JSON()
    var selectType = selectOrderType.delivery
    var arrSuburb:[JSON] = []
    var strSuburbId : String = ""
    var strSuburbCost : String = ""
    
    //MARK:- Outlet Zone
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var lblPhoneTitle: UILabel!
    @IBOutlet weak var txtPhone: UITextField!
    @IBOutlet weak var lblEmailTitle: UILabel!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var lblUnitNumberTitle: UILabel!
    @IBOutlet weak var txtUnitNumber: UITextField!
    @IBOutlet weak var lblStreetNumberTitle: UILabel!
    @IBOutlet weak var txtStreetNumber: UITextField!
    @IBOutlet weak var lblStreetNameTitle: UILabel!
    @IBOutlet weak var txtStreetName: UITextField!
    @IBOutlet weak var lblSuburbTitle: UILabel!
    @IBOutlet weak var txtSuburbName: UITextField!
    @IBOutlet weak var lblDeliveryInstructionTitle: UILabel!
    @IBOutlet weak var lblDeliveryInstructionPlaceholder: UILabel!
    @IBOutlet weak var txtviewDelivery: UITextView!
    @IBOutlet weak var lblGetGreatDealTitle: UILabel!
    @IBOutlet weak var lblReceiveViaEmail: UILabel!
    @IBOutlet weak var lblReceiveViaSMS: UILabel!
    @IBOutlet weak var lblRemeberPickupDetail: UILabel!
    @IBOutlet weak var btnViaEmailOutlet: UIButton!
    @IBOutlet weak var btnViaSMSOutlet: UIButton!
    @IBOutlet weak var btnPickupDetailOutlet: UIButton!
    @IBOutlet weak var btnNextOutlet: CustomButton!
    @IBOutlet weak var btnViaOuterEmailOutlet: UIButton!
    @IBOutlet weak var btnViaOuterSMSOutlet: UIButton!
    @IBOutlet weak var btnOuterPickupDetailOutlet: UIButton!
    
   // @IBOutlet weak var btnTermsOutlet: UIButton!
    @IBOutlet weak var viewOfUnitNumber: UIView!
    @IBOutlet weak var viewOfStreetNumber: UIView!
    @IBOutlet weak var viewOfStreetName: UIView!
    @IBOutlet weak var viewOfSuburb: UIView!
    @IBOutlet weak var viewOfInstruction: UIView!
    
    @IBOutlet var lblIAgreeAcceptTitle : UILabel!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
       
        setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: dict["store_name"].stringValue, type: .backWithCart, barType: .white)
    }

}

//MARK:- Setup UI

extension DeliveryDetailVC
{
    func setup()
    {
        lblTitle.textColor = UIColor.appThemeRedColor
        lblTitle.font = themeFont(size: 20, fontname: .semibold)
        lblTitle.text = getCommonString(key: "Enter_delivery_details_key").uppercased()
        
        [lblNameTitle,lblPhoneTitle,lblEmailTitle,lblUnitNumberTitle,lblStreetNumberTitle,lblStreetNameTitle
            ,lblSuburbTitle,lblDeliveryInstructionTitle,lblGetGreatDealTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .medium)
            lbl?.textColor = .black
        }
        
        [txtName,txtPhone,txtSuburbName,txtEmail,txtUnitNumber,txtStreetName,txtStreetNumber].forEach { (txtField) in
            txtField?.font = themeFont(size: 16, fontname: .regular)
            txtField?.textColor = .black
            txtField?.delegate = self
            txtField?.placeholder = getCommonString(key: "Enter_here_key")
        }
        
        lblNameTitle.text = getCommonString(key: "Name_key").uppercased()
        lblPhoneTitle.text = getCommonString(key: "Phone_key").uppercased()
        lblEmailTitle.text = getCommonString(key: "Email_key").uppercased()
        lblUnitNumberTitle.text = getCommonString(key: "Unit_number_key").uppercased()
        lblStreetNumberTitle.text = getCommonString(key: "Street_number_key").uppercased()
        lblStreetNameTitle.text = getCommonString(key: "Street_name_key").uppercased()
        lblSuburbTitle.text = getCommonString(key: "Suburb_key").uppercased()
        lblDeliveryInstructionTitle.text = getCommonString(key: "Delivery_instructions_key").uppercased()
        
        lblGetGreatDealTitle.text = getCommonString(key: "Get_great_deals_key").capitalized
        
        [lblReceiveViaEmail,lblReceiveViaSMS,lblRemeberPickupDetail].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .regular)
            lbl?.textColor = UIColor.appThemeLightGrayColor
        }
        
        lblReceiveViaEmail.text = getCommonString(key: "Receive_via_email_key")
        lblReceiveViaSMS.text = getCommonString(key: "Receive_via_sms_key")
        lblRemeberPickupDetail.text = getCommonString(key: "Remember_pickup_detail_key")
        
        lblDeliveryInstructionPlaceholder.textColor = UIColor.appThemeLightGrayColor
        lblDeliveryInstructionPlaceholder.font = themeFont(size: 16, fontname: .regular)
        lblDeliveryInstructionPlaceholder.text = getCommonString(key: "Enter_here_key")
        
        [txtviewDelivery].forEach { (txtView) in
            txtView?.font = themeFont(size: 16, fontname: .regular)
            txtView?.textColor = .black
            txtView?.delegate = self
            txtView?.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 8, right: 0)
        }
        
        btnNextOutlet.setUpThemeButtonUI()
        btnNextOutlet.setTitle(getCommonString(key: "Next_key"), for: .normal)
        
        addDoneButtonOnKeyboard(textfield: txtPhone)
        addDoneButtonOnKeyboard(textfield: txtUnitNumber)
        addDoneButtonOnKeyboard(textfield: txtStreetNumber)
       
        
        setUIForAgreeTermsPrivacy()
        
        if selectType == .pickup
        {
            
            self.lblTitle.text = getCommonString(key: "Enter_pickup_details_key").capitalized
            [viewOfUnitNumber,viewOfStreetName,viewOfStreetNumber,viewOfInstruction,viewOfSuburb].forEach { (view) in
                view?.isHidden = true
            }
        }else{
            getSuburb()
        }        
        
        if(getUserDetail("id") != "")
        {
            txtName.text = getUserDetail("username")
            txtPhone.text = getUserDetail("phone")
            txtEmail.text = getUserDetail("email_id")
        }
    }
    
}

//MARK:- Textfiled Delegate

extension DeliveryDetailVC:UITextFieldDelegate
{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtSuburbName
        {
            self.view.endEditing(true)
            let obj = CommonPickerVC()
            obj.selectedDropdown = .suburb
            obj.pickerDelegate = self
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .coverVertical
            obj.arrayPicker = arrSuburb
            self.present(obj, animated: false, completion: nil)
            return false
        }
        return true
    }
}

//MARK:- Delegate Method

extension DeliveryDetailVC:CommonPickerDelegate
{
    func setValuePicker(selectedDict: JSON) {        
        strSuburbId = selectedDict["id"].stringValue
        strSuburbCost = selectedDict["delivery-cost"].stringValue
        self.txtSuburbName.text = "\(selectedDict["location_name"].stringValue) (\(strSuburbCost))"
    }
    
}

//MARK:- Textview Delegate

extension DeliveryDetailVC:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblDeliveryInstructionPlaceholder.isHidden = false
        }
        else
        {
            self.lblDeliveryInstructionPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK:- Other Method
extension DeliveryDetailVC
{
    func setUIForAgreeTermsPrivacy()
    {
        //
        let str1 = getCommonString(key: "Terms_condition_title_key")
        let str2 = getCommonString(key: "Terms_condition_key")
        let str3 = getCommonString(key: "Privacy_policy_key").capitalized
        
        let str = str1 + " " + str2 + " \(getCommonString(key: "And_key")) " + str3
        let interactableText = NSMutableAttributedString(string:str)
        
        
        let rangeTerms = (str as NSString).range(of: str2, options: .caseInsensitive)
        let rangePolicy = (str as NSString).range(of: str3, options: .caseInsensitive)
        
        
        interactableText.addAttribute(NSAttributedString.Key.font,
                                      value: themeFont(size: 14, fontname: .regular),
                                      range: NSRange(location: 0, length: interactableText.length))
        
        
        interactableText.addAttribute(NSAttributedString.Key.foregroundColor,
                                      value: UIColor.appThemeRedColor, range: rangeTerms)
        
        
        interactableText.addAttribute(NSAttributedString.Key.foregroundColor,
                                      value: UIColor.appThemeRedColor, range: rangePolicy)
        
        lblIAgreeAcceptTitle.attributedText = interactableText
        
        
        let tapGeture = UITapGestureRecognizer(target: self, action: #selector(tapLabel))
        lblIAgreeAcceptTitle.addGestureRecognizer(tapGeture)
        
        
    }
    
    @objc func tapLabel(_ gesture: UITapGestureRecognizer)
    {
        let str1 = getCommonString(key: "Terms_condition_title_key")
        let str2 = getCommonString(key: "Terms_condition_key")
        let str3 = getCommonString(key: "Privacy_policy_key").capitalized
        let str = str1 + " " + str2 + " \(getCommonString(key: "And_key")) " + str3
        
        let rangeTerms = (str as NSString).range(of: str2, options: .caseInsensitive)
        let rangePolicy = (str as NSString).range(of: str3, options: .caseInsensitive)
        
        let checkClickedTerms = gesture.didTapAttributedTextInLabel(lblIAgreeAcceptTitle,targetRange : rangeTerms)
        
        if(checkClickedTerms)
        {
            print("Tapped terms")
            var dict = JSON()
            dict["name"].stringValue = getCommonString(key: "Terms_condition_key").capitalized
            dict["image"].stringValue = "ic_tnc_setting_screen"
           
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommonPolicyVC") as! CommonPolicyVC
            obj.dict = dict
            self.navigationController?.pushViewController(obj, animated: true)
            
        }
        
        let checkClickedPrivacy = gesture.didTapAttributedTextInLabel(lblIAgreeAcceptTitle,targetRange : rangePolicy)
        
        if(checkClickedPrivacy)
        {
            var dict = JSON()
            dict["name"].stringValue = getCommonString(key: "Privacy_policy_key").capitalized
            dict["image"].stringValue = "ic_privacy_policy_setting_screen"
            
            print("Tapped Privacy policy")
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "CommonPolicyVC") as! CommonPolicyVC
            obj.dict = dict
            self.navigationController?.pushViewController(obj, animated: true)
           
        }
        
    }
}

//MARK:- Action Zone

extension DeliveryDetailVC
{
    @IBAction func btnReceiveViaAction(_ sender:UIButton)
    {
        if sender == btnViaOuterSMSOutlet
        {
            btnViaSMSOutlet.isSelected = !btnViaSMSOutlet.isSelected
        }
        else if sender == btnViaOuterEmailOutlet
        {
            btnViaEmailOutlet.isSelected = !btnViaEmailOutlet.isSelected
        }
        else
        {
            btnPickupDetailOutlet.isSelected = !btnPickupDetailOutlet.isSelected
        }
    }
    
    @IBAction func btnNextAction(_ sender:UIButton)
    {
        objUser = User()
        objUser.strName = txtName.text ?? ""
        objUser.strPhoneNumber = txtPhone.text ?? ""
        objUser.strEmailAddress = txtEmail.text ?? ""
        objUser.strReceiveMail = btnViaEmailOutlet.isSelected ? "1" : "0"
        objUser.strReceiveSMS = btnViaEmailOutlet.isSelected ? "1" : "0"
        objUser.strRememberPickupDetil = btnPickupDetailOutlet.isSelected ? "1" : "0"
        
        if selectType == .delivery
        {
            objUser.strUnitNumber = txtUnitNumber.text ?? ""
            objUser.strStreetNumber = txtStreetNumber.text ?? ""
            objUser.strStreetName = txtStreetName.text ?? ""
            objUser.strSuburbName = txtSuburbName.text ?? ""
            objUser.strDeliveryInsturction = txtviewDelivery.text
            objUser.strSuburbId = strSuburbId
            objUser.strSuburbCost = strSuburbCost
        }
        
        if(selectType == .pickup)
        {
            if(!objUser.isCheckValidForPickupStore())
            {
                makeToast(message: objUser.strValidationMessage)
                return
            }
        }else{
            if(!objUser.isCheckValidForDeliveryStore())
            {
                makeToast(message: objUser.strValidationMessage)
                return
            }
        }
        
        SaveUserDetailsToDefaults()
        let obj = objStoryboard.instantiateViewController(withIdentifier: "OrderTimeVC") as! OrderTimeVC
        self.navigationController?.pushViewController(obj, animated: true)
        
    }
    
    override func btnCartAction(_ sender: UIButton) {
        objUser = User()
        objUser.strName = txtName.text ?? ""
        objUser.strPhoneNumber = txtPhone.text ?? ""
        objUser.strEmailAddress = txtEmail.text ?? ""
        objUser.strReceiveMail = btnViaEmailOutlet.isSelected ? "1" : "0"
        objUser.strReceiveSMS = btnViaEmailOutlet.isSelected ? "1" : "0"
        objUser.strRememberPickupDetil = btnPickupDetailOutlet.isSelected ? "1" : "0"
        
        if selectType == .delivery
        {
            objUser.strUnitNumber = txtUnitNumber.text ?? ""
            objUser.strStreetNumber = txtStreetNumber.text ?? ""
            objUser.strStreetName = txtStreetName.text ?? ""
            objUser.strSuburbName = txtSuburbName.text ?? ""
            objUser.strDeliveryInsturction = txtviewDelivery.text
            objUser.strSuburbId = strSuburbId
            objUser.strSuburbCost = strSuburbCost
        }
        
        if(selectType == .pickup)
        {
            if(!objUser.isCheckValidForPickupStore())
            {
                makeToast(message: objUser.strValidationMessage)
                return
            }
        }else{
            if(!objUser.isCheckValidForDeliveryStore())
            {
                makeToast(message: objUser.strValidationMessage)
                return
            }
        }
        
        SaveUserDetailsToDefaults()
        let obj = objStoryboard.instantiateViewController(withIdentifier: "CheckOutVC") as! CheckOutVC
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK:- Service

extension DeliveryDetailVC
{
    func getSuburb()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kSuburbList)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "store_id": dictStoreDetail["id"].stringValue
                
            ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrSuburb = []
                        self.arrSuburb = json["data"].arrayValue
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}

