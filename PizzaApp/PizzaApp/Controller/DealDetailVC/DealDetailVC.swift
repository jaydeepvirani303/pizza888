//
//  DealDetailVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 04/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class DealDetailVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dictData = JSON()
    var arrDealDetail:[JSON] = []
    var arrTmpDealDetail:[JSON] = []
    var dictCategoryData = JSON()
    var finalDealPrice:Float = 0.0
    var mainDealPrice:Float = 0.0
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblItemName:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var tblDealDetail:UITableView!
    @IBOutlet weak var heightOfAddtoCart: NSLayoutConstraint!
    @IBOutlet weak var btnAddToCartOutlet: UIButton!
    @IBOutlet weak var btnResetDealOutlet: UIButton!
    @IBOutlet weak var lblSelectedDeal: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var txtViewInstruction: CustomTextview!
    @IBOutlet weak var lblPlaceholder: UILabel!
    
    
    //MARK:- View Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        getDealList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: dictData["dealName"].stringValue, type: .backWithCart, barType: .white)
    }
    
    //MARK:- Setup
    
    func setupUI()  {
        
        [lblItemName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .semibold)
        }
        
        [lblPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeRedColor
            lbl?.font = themeFont(size: 22, fontname: .medium)
        }
        
        [lblDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 18, fontname: .regular)
        }
        
        mainDealPrice = dictData["dealPrice"].floatValue
        
        lblItemName.text = dictData["dealName"].stringValue
        lblPrice.text = "\(strCurrenrcy) \(dictData["dealPrice"].stringValue)"
        lblDescription.text = dictData["dealdiscription"].stringValue
        finalDealPrice = dictData["dealPrice"].floatValue
        
        hideAddtoCartButton()
        btnResetDealOutlet.isHidden = true
        
        [btnAddToCartOutlet,btnResetDealOutlet].forEach { (btn) in
            btn?.setTitleColor(UIColor.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .semibold)
            btn?.backgroundColor = UIColor.appThemeRedColor
        }
        
        btnAddToCartOutlet.setTitle(getCommonString(key: "Add_to_cart_key").capitalized, for: .normal)
        btnResetDealOutlet.setTitle(getCommonString(key: "Reset_deal_key").capitalized, for: .normal)
        
        imgProduct.layer.cornerRadius = 5
        imgProduct.layer.masksToBounds = true
        
        imgProduct.sd_setShowActivityIndicatorView(true)
        imgProduct.sd_setIndicatorStyle(.gray)
        imgProduct.sd_setImage(with: URL(string: dictData["image"].stringValue) , placeholderImage:UIImage(named:"ic_placeholder"), options: .lowPriority, completed: nil)
        
        [lblPlaceholder].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeLightGrayColor
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.text = getCommonString(key: "Enter_description_here_key")
        }
        
        [txtViewInstruction].forEach { (txtView) in
            txtView?.font = themeFont(size: 16, fontname: .regular)
            txtView?.textColor = .black
            txtView?.delegate = self
            txtView?.textContainerInset = UIEdgeInsets(top: 5, left: 8, bottom: 8, right: 8)
        }
        
    }

}

//MARK:- Textview Delegate

extension DealDetailVC:UITextViewDelegate
{
    func textViewDidChange(_ textView: UITextView) {
        if textView.text == ""
        {
            self.lblPlaceholder.isHidden = false
        }
        else
        {
            self.lblPlaceholder.isHidden = true
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn shouldChangeTextInRange: NSRange, replacementText text: String) -> Bool
    {
        if text == "\n"  // Recognizes enter key in keyboard
        {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
}

//MARK:- Private Zone

extension DealDetailVC
{
    func hideAddtoCartButton()
    {
//        self.heightOfAddtoCart.constant = 0
        self.btnAddToCartOutlet.isHidden = true
    }
    
    func showAddtoCartButton()
    {
        var isShowAddtoCartButton = true
        for i in 0..<arrDealDetail.count
        {
            let dict = arrDealDetail[i]
            if dict["is_added"].stringValue == "0"
            {
               isShowAddtoCartButton = false
            }
        }
        
        /*print("arrDealDetail - ",arrDealDetail)
        if(arrDealDetail.contains { (json) -> Bool in
            if(json["is_added"].stringValue == "0")
            {
                return false
            }
            return true
        })
        {
            self.heightOfAddtoCart.constant = 50
            self.btnAddToCartOutlet.isHidden = false
        }else{
            self.hideAddtoCartButton()
        }
        */
        if isShowAddtoCartButton
        {
//            self.heightOfAddtoCart.constant = 50
            self.btnAddToCartOutlet.isHidden = false
        }
        
    }
}


//MARK :- Action Zone

extension DealDetailVC
{
    @IBAction func btnAddtoCartAction(_ sender:UIButton)
    {        
        dictData["dealData"] = JSON(self.arrDealDetail)
        dictData["deal_extra_instruction"].stringValue = self.txtViewInstruction.text ?? ""
        print("Deal dict - ",dictData)
        AddToCart(dict: dictData)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnResetDealAction(_ sender:UIButton)
    {
        self.arrDealDetail = self.arrTmpDealDetail
        self.AddExtraInformation()
        self.tblDealDetail.reloadData()
        lblPrice.text = "\(strCurrenrcy) \(mainDealPrice)"
        dictData["dealPrice"].floatValue = mainDealPrice
        finalDealPrice = dictData["dealPrice"].floatValue
        hideAddtoCartButton()
        self.btnResetDealOutlet.isHidden = true
    }
    
    
    
    /*@IBAction func btnAddDealAction(_ sender:UIButton)
    {
        let section = Int(sender.title(for: .disabled)!)
        let row = sender.tag
        
        let arrInner = arrDealDetail[section!]["items"].arrayValue
        let strProductName = arrInner[row]["product_name"].stringValue
        
        var dictInner = arrDealDetail[section!]
        dictInner["name"].stringValue = strProductName
        dictInner["items"] = JSON([])
        dictInner["is_expand"] = "0"
        arrDealDetail[section!] = dictInner
        
        self.tblDealDetail.reloadSections(IndexSet(integer: section!), with: .none)
        
    }
    
    @IBAction func btnCustomizeAction(_ sender:UIButton)
    {
        let section = Int(sender.title(for: .disabled)!)
        let row = sender.tag
        
        let obj = objStoryboard.instantiateViewController(withIdentifier: "CustomizeVC") as! CustomizeVC
        var arrInner = arrDealDetail[section!]["items"].arrayValue
        obj.dictData = arrInner[row]
        obj.selectedCustomizedType = .deal
        obj.handledCustomize = {[weak self](dict) in
            var dictInner = self!.arrDealDetail[section!]
            dictInner["name"].stringValue = dict["product_name"].stringValue
            dictInner["items"] = JSON([])
            dictInner["ingr_add"].stringValue = dict["ingr_add"].stringValue
            dictInner["ingr_name"].stringValue = dict["ingr_name"].stringValue
            dictInner["is_expand"] = "0"
            self!.arrDealDetail[section!] = dictInner
            
            self!.tblDealDetail.reloadSections(IndexSet(integer: section!), with: .none)
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnExpandAction(_ sender:UIButton)
    {
        var dictInner = arrDealDetail[sender.tag]
        if dictInner["is_expand"].stringValue == "0"
        {
            let dictMain = arrTmpDealDetail[sender.tag]
            dictInner["items"] = JSON(dictMain["items"])
            dictInner["is_expand"] = "1"
        }
        else
        {
            dictInner["items"] = JSON([])
            dictInner["is_expand"] = "0"
        }
        self.arrDealDetail[sender.tag] = dictInner
        self.tblDealDetail.reloadData()
    }*/
}

//MARK:- Tableview Delegate & Datasource

extension DealDetailVC :UITableViewDelegate,UITableViewDataSource
{
   /* func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let vw = Bundle.main.loadNibNamed("ViewDealSection", owner: self, options: nil)?[0] as? ViewDealSection
        let strItemName = "\(arrDealDetail[section]["name"].stringValue) \n Add : \(arrDealDetail[section]["ingr_name"].stringValue)"
        vw?.lblItmeName.text = strItemName        
        
        vw?.btnExpandOutetOutlet.isUserInteractionEnabled  = true
        if arrDealDetail[section]["is_expand"].stringValue == "0"
        {
            vw?.btnExpandOutlet.isSelected = false
        }
        else if arrDealDetail[section]["is_expand"].stringValue == "1"
        {
            vw?.btnExpandOutlet.isSelected = true
        }
        else if arrDealDetail[section]["is_expand"].stringValue == "2"
        {
            vw?.btnExpandOutlet.isEnabled = true           
        }
        vw?.btnExpandOutetOutlet.tag = section
        vw?.btnExpandOutetOutlet.addTarget(self, action: #selector(btnExpandAction(_:)), for: .touchUpInside)
        return vw
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrDealDetail.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {        
        return arrDealDetail[section]["items"].arrayValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DealDetailCell") as! DealDetailCell
        let arrInner = arrDealDetail[indexPath.section]["items"].arrayValue
        cell.lblItemName.text = arrInner[indexPath.row]["product_name"].stringValue
        if arrInner[indexPath.row]["is_customize"].stringValue == "0"
        {
            cell.viewOfCustomize.isHidden = true
        }else
        {
            cell.viewOfCustomize.isHidden = false
        }
        cell.btnAddDealOutlet.setTitle("\(indexPath.section)", for: .disabled)
        cell.btnAddDealOutlet.tag = indexPath.row
        
        cell.btnCustomizeOutlet.setTitle("\(indexPath.section)", for: .disabled)
        cell.btnCustomizeOutlet.tag = indexPath.row
        return cell
    }*/
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrDealDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductListCell") as! ProductListCell
        let dict = arrDealDetail[indexPath.row]
        print("dict\(dict)")
        cell.lblItemName.text = dict["name"].stringValue + " \(dict["addedVariant"].stringValue.isEmpty == true ? "" : "(\((dict["addedVariant"].stringValue)))")"

        var strToppingsName = ""
//        var strToppingsName2 : NSAttributedString?
        if(dict["ingr_name"].stringValue != "")
        {
            cell.lblItemName.text = dict["name"].stringValue + " \(dict["addedVariant"].stringValue.isEmpty == true ? "" : "(\((dict["addedVariant"].stringValue)))")"

            strToppingsName = "Add - \(dict["ingr_name"].stringValue)\n"
            
//            strToppingsName1 = attributedString(string1: "Add - ", string2: "\(dict["ingr_name"].stringValue)", color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeBlackColor, font1: themeFont(size: 14, fontname: .semibold), font2: themeFont(size: 12, fontname: .regular))
            
        }
        if(dict["remove_ingr_name"].stringValue != "")
        {
            cell.lblItemName.text = dict["name"].stringValue + " \(dict["addedVariant"].stringValue.isEmpty == true ? "" : "(\((dict["addedVariant"].stringValue)))")" // + " (\(dict["pizzaSize"].stringValue))"

            strToppingsName = strToppingsName + "Remove - \(dict["remove_ingr_name"].stringValue)"
//            strToppingsName2 = attributedString(string1: "Remove - ", string2: "\(dict["remove_ingr_name"].stringValue)", color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeBlackColor, font1: themeFont(size: 14, fontname: .semibold), font2: themeFont(size: 12, fontname: .regular))

        }
        cell.lblSelectedTopping.text = strToppingsName
//        cell.lblSelectedTopping.attributedText = attributedString(string1: strToppingsName1, string2: strToppingsName2, color1: UIColor.appThemeBlackColor, color2: UIColor.appThemeBlackColor, font1: themeFont(size: 18, fontname: .semibold), font2: <#T##UIFont#>)
        
//        let myAttribute = [NSAttributedString.Key.foregroundColor: UIColor.appThemeBlackColor]
//        let myAttrString = NSAttributedString(string: "\(String(describing: strToppingsName1))\n\(String(describing: strToppingsName2))", attributes: myAttribute)
//
//        cell.lblSelectedTopping.attributedText = myAttrString
//
       
        
        if dict["is_added"] == "0"
        {
            cell.btnDisclosure.isSelected = true
            cell.contentView.alpha = 1
        }else
        {
            cell.btnDisclosure.isSelected = false
            cell.contentView.alpha = 0.5
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrDealDetail[indexPath.row]
        if dict["is_added"] == "1"{
            return
        }
        let obj = objStoryboard.instantiateViewController(withIdentifier: "ProductListVC") as! ProductListVC
        obj.dictData = arrDealDetail[indexPath.row]
        obj.dictDealData = dictData
        obj.handleAddToDeal  = {[weak self](dict,tag)  in
            
            self?.btnResetDealOutlet.isHidden = false
            print("Selected ingridentans dict data - ",dict)
            
            var dictInner = self!.arrDealDetail[indexPath.row]
            dictInner["name"].stringValue = dict["product_name"].stringValue
            dictInner["product_name"].stringValue = dict["product_name"].stringValue

            dictInner["product_id"].stringValue = dict["product_id"].stringValue
            dictInner["is_added"] = "1"
            dictInner["half_price"].stringValue = dict["half_price"].stringValue
            dictInner["small_price"].stringValue = dict["small_price"].stringValue
            dictInner["price"].stringValue = dict["price"].stringValue
            dictInner["description"].stringValue = dict["description"].stringValue
            dictInner["specification"].stringValue = dict["specification"].stringValue
            dictInner["type"].stringValue = dict["type"].stringValue
            dictInner["customized"].stringValue = dict["customized"].stringValue
            dictInner["category_name"].stringValue = dict["category_name"].stringValue
            dictInner["category_id"].stringValue = dict["category_id"].stringValue

            if tag == 2
            {
                dictInner["ingr_add"].stringValue = dict["ingr_add"].stringValue
                dictInner["ingr_name"].stringValue = dict["ingr_name"].stringValue
                dictInner["remove_ingr_id"].stringValue = dict["remove_ingr_id"].stringValue
                dictInner["remove_ingr_name"].stringValue = dict["remove_ingr_name"].stringValue
                dictInner["addedTopingsPrice"].stringValue = dict["addedTopingsPrice"].stringValue
                dictInner["sauce_type"].stringValue = dict["sauce_type"].stringValue
                dictInner["pizzaSize"].stringValue = dict["pizzaSize"].stringValue
                dictInner["defaultTypeId"].stringValue = dict["defaultTypeId"].stringValue
                dictInner["baseType"].stringValue = dict["baseType"].stringValue
                
                dictInner["finalPrice"].stringValue = dict["finalPrice"].stringValue
                dictInner["addedVariant"].stringValue = dict["addedVariant"].stringValue
                dictInner["quantity"].stringValue = dict["quantity"].stringValue
                
                self?.finalDealPrice += dict["finalPrice"].floatValue
//                let addedTopingsPrice = dict["addedTopingsPrice"].floatValue + (self?.dictData["dealPrice"].floatValue)! + dict["finalPrice"].floatValue
                self?.lblPrice.text = "\(strCurrenrcy) \(String(format: "%0.2f", self?.finalDealPrice ?? 0.0))"
                self?.dictData["dealPrice"].stringValue = "\(self?.finalDealPrice ?? 0.0)"
            }
            print("dictInner \(dictInner)")
            self!.arrDealDetail[indexPath.row] = dictInner
            self!.tblDealDetail.reloadRows(at: [indexPath], with: .none)
            self!.showAddtoCartButton()
        }
        self.navigationController?.pushViewController(obj, animated: true)
    }
}
//MARK:- Other methods
extension DealDetailVC
{
    func AddExtraInformation()
    {
        
        for i in 0..<self.arrDealDetail.count
        {
            var dictInner = self.arrDealDetail[i]
            dictInner["is_added"] = "0"
            dictInner["category_name"].stringValue = dictCategoryData["category_name"].stringValue

            var arrayItemsLocal = dictInner["items"].arrayValue
            for j in 0..<arrayItemsLocal.count
            {
                arrayItemsLocal[j] = CheckType(json:arrayItemsLocal[j])
            }
            dictInner["items"] = JSON(arrayItemsLocal)
            self.arrDealDetail[i] = CheckType(json:dictInner)
        }
        
        print("arrDealDetail - ",self.arrDealDetail)
    }
    
    func CheckType(json:JSON) -> JSON
    {
        var dict = json
        print("DEal product dict - ",dict)
        print("DEal product type - ",dict["type"].intValue)

        
        switch dict["type"].intValue
        {
            case 1:
                return dict
            case 5:
                let arrayPastaBase = getPastaBaseNames()
            
                if(arrayPastaBase.count > 0)
                {
                    dict["pastaBase"].stringValue = arrayPastaBase.first ?? "Select Base"
                    dict["defaultTypeId"].stringValue = "1"
                    
                    
                }
                return dict
            case 2:
                
                dict["sauce_type"].stringValue =  ""
                dict["pizzaSize"].stringValue = ""
                dict["extra_description"].stringValue =  ""
                //                dict["topping"].stringValue = ""
                dict["is_customized"].stringValue = "0"
                dict["defaultTypeId"].stringValue = "1"
                
                
                return dict
                
            case 3:
                
                
                    dict["is_customized"].stringValue = "0"
                    //                dict["defaultTypeId"].stringValue = "1"
                    //                dict["sauce_type"].stringValue =  ""
                    //                dict["pizzaSize"].stringValue = ""
                    //                dict["extra_description"].stringValue =  ""
                    return dict
            
            case 6:
                
                    dict["is_customized"].stringValue = "0"
                    dict["defaultTypeId"].stringValue = "1"
                    dict["sauce_type"].stringValue =  ""
                    dict["pizzaSize"].stringValue = ""
                    dict["extra_description"].stringValue =  ""
                    return dict
            
            default:
                return dict
        }
    }
}
//MARK:- Service

extension DealDetailVC
{
    func getDealList()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kProductCustomize)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "type":dictData["type"].stringValue,
                          "store_id": dictStoreDetail["id"].stringValue,
                          "cst_id":dictData["dealid"].stringValue]
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        let dict = json["data"]
                        self.arrDealDetail = []
                        print("Deal sub array data - ",dict["sub_data"].arrayValue)
                        self.arrTmpDealDetail = dict["sub_data"].arrayValue
                        self.arrDealDetail = dict["sub_data"].arrayValue
                        self.AddExtraInformation()
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblDealDetail.reloadData()
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    
}




