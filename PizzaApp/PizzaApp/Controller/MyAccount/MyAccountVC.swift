//
//  MyAccountVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 25/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON


class MyAccountVC: UIViewController {
    
    //MARK:- Variable declaration
    
    var arrAccount:[JSON] = []
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var tblAccount: UITableView!
    @IBOutlet weak var heightTblAccount: NSLayoutConstraint!
    
    //MARK:- ViewLife Cycle

    override func viewDidLoad() {
        super.viewDidLoad()

       setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
       
        tblAccount.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "My_account_key").capitalized, type: .sidemenu, barType: .white)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblAccount.removeObserver(self, forKeyPath: "contentSize")
        
    }
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblAccount.contentSize.height)")
            self.heightTblAccount.constant = tblAccount.contentSize.height
        }
    }
   

}

//MARK:- Array Setup

extension MyAccountVC
{
    func setupOrderStatusArray() -> [JSON]
    {
        var arrMenu:[JSON] = []
        
        var dict = JSON()
        dict["name"].stringValue = getCommonString(key: "My_profile_key").capitalized
        dict["image"].stringValue = "ic_my_profile_account_screen_black"
        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "My_orders_key").capitalized
        dict["image"].stringValue = "ic_my_order_account_screen_black"
        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Change_password_key").capitalized
        dict["image"].stringValue = "ic_password_account_screen_black"
        arrMenu.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = getCommonString(key: "Logout_key").capitalized
        dict["image"].stringValue = "ic_login_account_screen_black"
        arrMenu.append(dict)
        
        return arrMenu
    }
}

//MARK:- Setup UI
extension MyAccountVC
{
    func setupUI()
    {
        
        [lblUserName].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .semibold)
            lbl?.textColor = UIColor.appThemeRedColor
        }
        
        lblUserName.text = "\(getCommonString(key: "Welcome_key")) : \(getUserDetail("username"))"
        
        arrAccount = setupOrderStatusArray()
        self.tblAccount.reloadData()
        self.tblAccount.tableFooterView = UIView()
    }
}


//MARK:- Tableview Delegate & Datasource

extension MyAccountVC : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAccount.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyAccountCell") as! MyAccountCell
        cell.setData(arrAccount[indexPath.row])
        if indexPath.row == 0
        {
            cell.vwUpperSep.isHidden = false
        }
        else
        {
            cell.vwUpperSep.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = arrAccount[indexPath.row]
        if getCommonString(key: "My_profile_key").lowercased() == dict["name"].stringValue.lowercased()
        {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if getCommonString(key: "Change_password_key").lowercased() == dict["name"].stringValue.lowercased()
        {
            let obj = objStoryboard.instantiateViewController(withIdentifier: "ChangePasswordVC") as! ChangePasswordVC
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if getCommonString(key: "My_orders_key").lowercased() == dict["name"].stringValue.lowercased()
        {
//            let obj = objStoryboard.instantiateViewController(withIdentifier: "MyOrderParentVC") as! MyOrderParentVC
            let obj = objStoryboard.instantiateViewController(withIdentifier: "MyOrderVC") as! MyOrderVC
            obj.selectedHistoryType = selectMyOrderType.past
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if getCommonString(key: "Logout_key").lowercased() == dict["name"].stringValue.lowercased()
        {
            let alertController = UIAlertController(title: getCommonString(key: "Pizzaapp_key"), message: getCommonString(key: "Do_you_want_to_logout_from_the_app_key"), preferredStyle: UIAlertController.Style.alert)
            
            let okAction = UIAlertAction(title: getCommonString(key: "Yes_key"), style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                
               self.logoutAPICalling()
                //self.userLogout()
                
            }
            let cancelAction = UIAlertAction(title: getCommonString(key: "No_key"), style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
                print("Cancel")
            }
            alertController.addAction(okAction)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    
}
