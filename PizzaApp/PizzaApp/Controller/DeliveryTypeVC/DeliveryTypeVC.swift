//
//  DeliveryTypeVC.swift
//  PizzaApp
//
//  Created by Jaydeep on 21/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class DeliveryTypeVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var dict = JSON()
    var isComeFromA = String()
    var arrDeliveryType = [JSON]()
    var arrServiceType = [JSON]()
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPickup: UILabel!
    @IBOutlet weak var lblDelivery: UILabel!
    @IBOutlet weak var lblReorder: UILabel!
    @IBOutlet weak var tblDeliveryType: UITableView!
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
        getDeliveryType()
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: dict["store_name"].stringValue, type: .backWithCart, barType: .black)
    }
    
    //MARK:- Setup
    
    func setupUI()  {
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .regular)
            lbl?.textColor = UIColor.white
        }
        lblTitle.text = getCommonString(key: "How_can_we_help_you_key").uppercased()
        
        [lblPickup,lblReorder,lblDelivery].forEach { (lbl) in
            lbl?.font = themeFont(size: 22, fontname: .semibold)
            lbl?.textColor = UIColor.black
        }
        
        lblPickup.text = getCommonString(key: "Pickup_key")
        lblDelivery.text = getCommonString(key: "Delivery_key")
        lblReorder.text = getCommonString(key: "Reorder_key")
    }
    
}

//MARK:- Action Zone
extension DeliveryTypeVC
{
    @IBAction func btnPickupAction(_ sender:UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "DeliveryDetailVC") as! DeliveryDetailVC
        obj.selectType = .pickup
        obj.dict = dict
        objOrder.strSelectedOrderType = selectOrderType.pickup
        objUser = User()
        objUser.strUnitNumber = ""
        objUser.strStreetNumber = ""
        objUser.strStreetName = ""
        objUser.strSuburbName = ""
        objUser.strDeliveryInsturction = ""
        objUser.strSuburbId = ""
        objUser.strSuburbCost = ""
        SaveOrderDetailsToDefaults()
        intDeliveryType = selectOrderType(rawValue: selectOrderType.pickup.rawValue)!
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnDelivryAction(_ sender:UIButton)
    {
        let obj = objStoryboard.instantiateViewController(withIdentifier: "DeliveryDetailVC") as! DeliveryDetailVC
        obj.selectType = .delivery
        obj.dict = dict
        objOrder.strSelectedOrderType = selectOrderType.delivery
        SaveOrderDetailsToDefaults()

        intDeliveryType = selectOrderType(rawValue: selectOrderType.delivery.rawValue)!
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnReoderAction(_ sender:UIButton)
    {
         
    }
}

//MARK:- Tablevide Delegate

extension DeliveryTypeVC:UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrDeliveryType.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrDeliveryType[section]["is_selected"].stringValue == "0" {
            return 0
        }
        return arrDeliveryType[section]["data"].arrayValue.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeliveryTypeCell") as! DeliveryTypeCell
        let arr = arrDeliveryType[indexPath.section]["data"].arrayValue
        let dict = arr[indexPath.row]
        cell.lblTitle.text = dict["servive_name"].stringValue
        cell.viewSelection.isHidden = dict["is_selected"].stringValue == "1" ? false : true
//        cell.imgType?.image = indexPath.row%2 == 0 ? UIImage(named: "ic_pickup_select_service_screen") : UIImage(named: "ic_delivery_select_service_screen")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let arrayCart = CheckoutOrder().getOrderData()
        let arrCheckCartItem = GetCartData()
        print("arrayCart \(arrayCart)")
        for i in 0..<self.arrDeliveryType.count {
            var dictOuter = self.arrDeliveryType[i]
            var arr = dictOuter["data"].arrayValue
            for j in 0..<arr.count {
                var dictInner = arr[j]
                dictInner["is_selected"].stringValue = "0"
                arr[j] = dictInner
            }
            dictOuter["data"] = JSON(arr)
            self.arrDeliveryType[i] = dictOuter
        }
        var arr = arrDeliveryType[indexPath.section]["data"].arrayValue
        var dict = arr[indexPath.row]
        dict["is_selected"].stringValue = "1"
        arr[indexPath.row] = dict
        arrDeliveryType[indexPath.section]["data"] = JSON(arr)
        objOrder.strProductType = arrDeliveryType[indexPath.section]["display_product_type"].stringValue
        objOrder.strServiceType = dict["service_type"].stringValue
        strProductType = arrDeliveryType[indexPath.section]["display_product_type"].stringValue
        strServiceType = dict["service_type"].stringValue
     //   objOrder.strSelectedOrderType = arrDeliveryType[indexPath.section]["display_product_type"].intValue
        if indexPath.section == 0 {
            if arrCheckCartItem.count != 0 {
                let objOrder = arrayCart[0]
                if objOrder.strSelectedOrderType == 1 {
                    displayAlertWithTitle(getCommonString(key: "Pizzaapp_key"), andMessage: "If you will change delivery type then your previous item will be remove.Are you sure want to continue?", buttons: ["Yes","No"], completion: {(tag) in
                        if tag == 0 {
                            self.deleteAllItemsfromCart()
                            self.btnPickupAction(UIButton())
                        }
                    })
                } else {
                    self.btnPickupAction(UIButton())
                }
                
            } else {
                self.btnPickupAction(UIButton())
            }
        } else {
            if arrCheckCartItem.count != 0 {
                let objOrder = arrayCart[0]
                if objOrder.strSelectedOrderType == 2 {
                    displayAlertWithTitle(getCommonString(key: "Pizzaapp_key"), andMessage: "If you will change delivery type then your previous item will be remove.Are you sure want to continue?", buttons: ["Yes","No"], completion: {(tag) in
                        if tag == 0 {
                            self.deleteAllItemsfromCart()
                            self.btnDelivryAction(UIButton())
                        }
                    })
                } else {
                    self.btnDelivryAction(UIButton())
                }
            } else {
                self.btnDelivryAction(UIButton())
            }
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view : DeliveryHeaderCell = .fromNib()
        let dict = arrDeliveryType[section]
        view.lblTitle.text = dict["name"].stringValue
        view.imgType?.image = section%2 == 0 ? UIImage(named: "ic_pickup_select_service_screen") : UIImage(named: "ic_delivery_select_service_screen")
        view.btnSelectTypeOutlet.tag = section
        view.btnSelectTypeOutlet.addTarget(self, action: #selector(btnSelectTypeAction(_:)), for: .touchUpInside)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 110
    }
    
    @IBAction func btnSelectTypeAction(_ sender:UIButton) {
        for i in 0..<self.arrDeliveryType.count {
            var dict = self.arrDeliveryType[i]
            dict["is_selected"].stringValue = "0"
            self.arrDeliveryType[i] = dict
        }
        var dict = self.arrDeliveryType[sender.tag]
        dict["is_selected"].stringValue = "1"
        self.arrDeliveryType[sender.tag] = dict
        self.tblDeliveryType.reloadData()
        
    }
    
}

//MARK:- API

extension DeliveryTypeVC {
    func getDeliveryType()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kDeliveryType)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang,
                          "store_id":objOrder.strSelectedStoreId
            ]
               
            print("Param : \(param)")
            
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrDeliveryType = json["data"]["type"].arrayValue
                        self.arrServiceType = json["data"]["service"].arrayValue
                        for i in 0..<self.arrDeliveryType.count {
                            var dict = self.arrDeliveryType[i]
                            dict["data"] = JSON(self.arrServiceType)
                            dict["is_selected"] = "0"
                            self.arrDeliveryType[i] = dict
                        }
                        self.tblDeliveryType.reloadData()
                    }
                    else
                    {
                        makeToast(message: json["msg"].stringValue)
                    }
                    
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}
