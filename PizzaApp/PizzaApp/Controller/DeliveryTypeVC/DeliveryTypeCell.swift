//
//  DeliveryTypeCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 27/11/20.
//  Copyright © 2020 Pizza 888. All rights reserved.
//

import UIKit

class DeliveryTypeCell: UITableViewCell {

    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var viewSelection:CustomView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 22, fontname: .semibold)
            lbl?.textColor = UIColor.white
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
