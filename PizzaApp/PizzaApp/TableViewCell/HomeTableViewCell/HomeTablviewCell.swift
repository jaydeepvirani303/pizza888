//
//  HomeTablviewCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 20/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class HomeTablviewCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var vwOuter: UIView!
    
    //MARK:- ViewLife Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblShopName].forEach { (lbl) in
            lbl?.font = themeFont(size: 20, fontname: .semibold)
            lbl?.textColor = UIColor.appThemeBlackColor
        }
        
        
        // Initialization code
    }
    
    override func draw(_ rect: CGRect) {
        vwOuter.roundCorners([.topRight,.bottomRight], radius: 30)
    }
    
    override func layoutSubviews() {        
     
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(_ dict:JSON)  {
        lblShopName.text = dict["store_name"].stringValue
    }
    
}
