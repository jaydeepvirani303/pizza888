//
//  HalfChildCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 08/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class HalfChildCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblHalfName: UILabel!
    @IBOutlet weak var btnSelectHalfOutlet: UIButton!
    @IBOutlet weak var btnCustomizeOutelt: UIButton!
    @IBOutlet weak var lblHalfDescription: UILabel!
    
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    
    
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        [lblHalfName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .semibold)
        }
        
        [lblPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeRedColor
            lbl?.font = themeFont(size: 18, fontname: .medium)
        }
        
        [lblHalfDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 18, fontname: .regular)
        }
        
        [btnCustomizeOutelt].forEach { (btn) in
            btn?.setTitleColor(UIColor.appThemeRedColor, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .semibold)
        }
       
        btnCustomizeOutelt.setTitle(getCommonString(key: "Customize_key"), for: .normal)
        
        imgProduct.layer.cornerRadius = 5
        imgProduct.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
