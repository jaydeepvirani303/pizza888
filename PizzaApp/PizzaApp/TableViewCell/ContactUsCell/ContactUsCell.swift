//
//  ContactUsCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 18/06/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class ContactUsCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblStoreAddress: UILabel!
    @IBOutlet weak var lblStoreName: UILabel!
    @IBOutlet weak var lblStoreMobileNumber: UILabel!
    @IBOutlet weak var lblEmailAddress: UILabel!
    
    //MARK:- View Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        /*[lblStoreAddress,lbl].forEach { (lbl) in
            lbl?.font = themeFont(size: 14, fontname: .medium)
            lbl?.textColor = UIColor.appThemeLightGrayColor
        }*/
        
        [lblStoreName].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .medium)
            lbl?.textColor = UIColor.appThemeRedColor
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setUpStoreLocations(dict:JSON)
    {
        lblStoreName.text = dict["store_name"].stringValue        
    }

}
