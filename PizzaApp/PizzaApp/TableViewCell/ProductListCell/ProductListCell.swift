//
//  ProductListCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 11/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class ProductListCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var btnDisclosure: UIButton!
    
    @IBOutlet weak var lblSelectedTopping: UILabel!
    
    
    //MARK:- View Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        [lblItemName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .semibold)
        }
        
        [lblSelectedTopping].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 14, fontname: .regular)
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
