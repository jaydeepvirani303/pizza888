//
//  AddOnsCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 22/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class AddOnsCell: UITableViewCell {
    
    //MARK:- Outket ZOne
    
    @IBOutlet weak var btnSelectAddOnsOutlet: UIButton!
    @IBOutlet weak var lblAddOnsName: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    //MARK:- View LifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblAddOnsName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 18, fontname: .regular)
        }
        
        [lblPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .regular)
        }
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setAddOnData(dict:JSON)  {
        self.lblAddOnsName.text = dict["ingredient_name"].stringValue
        self.lblPrice.text = "\(strCurrenrcy) \(dict["ingredient_price"].stringValue)"
        if dict["is_selected"].stringValue == "0"
        {
            self.btnSelectAddOnsOutlet.isSelected = false
        }
        else{
            self.btnSelectAddOnsOutlet.isSelected = true
        }
    }

}
