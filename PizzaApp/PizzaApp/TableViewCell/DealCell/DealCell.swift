//
//  DealCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 18/01/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SkeletonView
import SwiftyJSON

class DealCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblItemName: UILabel!
    @IBOutlet weak var lblItemPrice: UILabel!
    @IBOutlet weak var lblItemDescription: UILabel!
    @IBOutlet weak var lblCustomizeType: UILabel!
    @IBOutlet weak var vwCustomize: CustomView!
    @IBOutlet weak var btnCustomizedOutlet: UIButton!
    @IBOutlet weak var btnCustomizeSelectOutlet: UIButton!
    @IBOutlet weak var imgvwProduct: UIImageView!

    //MARK:- View Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        [lblItemName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .semibold)
        }
        
        [lblItemPrice].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeRedColor
            lbl?.font = themeFont(size: 22, fontname: .medium)
        }
        
        [lblItemDescription].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeDarkGrayColor
            lbl?.font = themeFont(size: 18, fontname: .regular)
        }
       
        
        [lblCustomizeType].forEach { (lbl) in
            lbl?.font = themeFont(size: 15, fontname: .regular)
        }
        
        [lblItemName,lblItemDescription].forEach { (lbl) in
            lbl?.showAnimatedSkeleton()
        }
        
        btnCustomizeSelectOutlet.isUserInteractionEnabled = false
        
        imgvwProduct.layer.cornerRadius = 5
        imgvwProduct.layer.masksToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func stopAnimationSkeleton()
    {
        [lblItemName,lblItemPrice,lblItemDescription,lblCustomizeType].forEach { (lbl) in
            lbl?.hideSkeleton()
        }
       
        
    }
    
    func setDataForDeal(dict:JSON)
    {
        lblItemName.text = dict["dealName"].stringValue
        lblItemPrice.text = "\(strCurrenrcy) \(dict["dealPrice"].stringValue)"
        lblItemDescription.text = dict["dealdiscription"].stringValue
        
        if dict["is_customized"].stringValue == "1"
        {
            vwCustomize.borderColor = UIColor.appThemeGreenColor
            lblCustomizeType.textColor  = UIColor.appThemeGreenColor
            btnCustomizedOutlet.isSelected = true
            lblCustomizeType.text = getCommonString(key: "Customize_key")
        }
        else
        {
            vwCustomize.borderColor = UIColor.appThemeRedColor
            lblCustomizeType.textColor  = UIColor.appThemeRedColor
            btnCustomizedOutlet.isSelected = false
            lblCustomizeType.text = "\(getCommonString(key: "Customize_key"))"
        }
    }
    
    
    func setDataForHalf(dict:JSON)
    {
        lblItemName.text = dict["halfName"].stringValue
        lblItemPrice.text = "\(strCurrenrcy) \(dict["halfPrice"].stringValue)"
        lblItemDescription.text = dict["halfdiscription"].stringValue
        
        if dict["is_customized"].stringValue == "1"
        {
            vwCustomize.borderColor = UIColor.appThemeGreenColor
            lblCustomizeType.textColor  = UIColor.appThemeGreenColor
            btnCustomizedOutlet.isSelected = true
            lblCustomizeType.text = getCommonString(key: "Customize_key")
        }
        else
        {
            vwCustomize.borderColor = UIColor.appThemeRedColor
            lblCustomizeType.textColor  = UIColor.appThemeRedColor
            btnCustomizedOutlet.isSelected = false
            lblCustomizeType.text = "\(getCommonString(key: "Customize_key"))"
        }
    }


}
