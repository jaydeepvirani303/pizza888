//
//  DeliveryHeaderCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 30/11/20.
//  Copyright © 2020 Pizza 888. All rights reserved.
//

import UIKit

class DeliveryHeaderCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var imgType:UIImageView!
    @IBOutlet weak var btnSelectTypeOutlet:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 22, fontname: .semibold)
            lbl?.textColor = UIColor.black
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
