//
//  HomeCollectionCellCollectionViewCell.swift
//  PizzaApp
//
//  Created by Jaydeep on 20/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage
class HomeCollectionCell: UICollectionViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var imgOfHeader: UIImageView!
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setData(_ dict:JSON) {
        
//        imgOfHeader.image = UIImage(named: dict["name"].stringValue)
        let strURL = dict["bannerUrl"].stringValue
        imgOfHeader.sd_setShowActivityIndicatorView(true)
        imgOfHeader.sd_setIndicatorStyle(.gray)
        imgOfHeader.sd_setImage(with: URL(string: strURL) , placeholderImage:UIImage(named:"banner_home_page.png"), options: .lowPriority, completed: nil)
    }

}
