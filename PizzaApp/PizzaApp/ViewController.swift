//
//  ViewController.swift
//  PizzaApp
//
//  Created by Jaydeep on 19/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import CHIPageControl
import SwiftyJSON
import Alamofire
import AlamofireSwiftyJSON

class ViewController: UIViewController {
    
    //MARK:- Variable Declaration
    
    var arrHome:[JSON] = []
    var arrHeaderImage:[JSON] = []
    {
        didSet {
            if selectedIndex == -1 || self.selectedIndex == 0
            {
                self.btnPreviousOutlet.isHidden = true
            }else{
                [btnPreviousOutlet,btnNextOutlet].forEach { $0?.isHidden = arrHeaderImage.count < 2 }
            }
            
        }
    }
    var selectedIndex = 0
    {
        didSet
        {
            if(arrHeaderImage.count > 0)
            {
                btnPreviousOutlet.isHidden = selectedIndex == 0
                btnNextOutlet.isHidden = selectedIndex == (arrHeaderImage.count - 1)
            }else{
                btnPreviousOutlet.isHidden = true
                btnNextOutlet.isHidden = true
            }
            
        }
    }
    
    //MARK:- Outlet Zone
    @IBOutlet weak var collectionOfHeader: UICollectionView!
    @IBOutlet weak var pageController: CHIPageControlJaloro!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblHome: UITableView!
    @IBOutlet weak var heightOfTable: NSLayoutConstraint!
    @IBOutlet weak var btnNextOutlet: UIButton!
    @IBOutlet weak var btnPreviousOutlet: UIButton!
    @IBOutlet weak var vwContent: UIView!

    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        printFonts()       
        
        setupUI()
        
        getBannerImages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tblHome.addObserver(self, forKeyPath: "contentSize", options: [.new], context: nil)
        sideMenuController?.isLeftViewSwipeGestureEnabled = true
        setUpNavigationBarWithTitleAndSideMenuAndCart(strTitle: getCommonString(key: "Home_key"), type: .sideMenuWithCart, barType: .black)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        tblHome.removeObserver(self, forKeyPath: "contentSize")
        sideMenuController?.isLeftViewSwipeGestureEnabled = false
        
    }

    
    
    //MARK:- Overide Method
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if object is UITableView {
            print("contentSize:= \(tblHome.contentSize.height)")
            self.heightOfTable.constant = tblHome.contentSize.height
        }
    }

}



//MARK:- Setup UI
extension ViewController
{
    func setupUI()
    {
        vwContent.isHidden = true
        collectionOfHeader.register(UINib(nibName: "HomeCollectionCell", bundle: nil), forCellWithReuseIdentifier: "HomeCollectionCell")
        
        tblHome.register(UINib(nibName: "HomeTablviewCell", bundle: nil), forCellReuseIdentifier: "HomeTablviewCell")
        
        getStoreList()
        //arrHome = setupHomeArray()
        
        [lblTitle].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .semibold)
            lbl?.textColor = UIColor.white
        }
        
        lblTitle.text = getCommonString(key: "Select_our_store_key").uppercased()
        
        /*var dict = JSON()
        dict["name"].stringValue = "1.jpeg"
        arrHeaderImage.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "2.jpeg"
        arrHeaderImage.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "3.jpeg"
        arrHeaderImage.append(dict)*/
        
        
//        self.collectionOfHeader.reloadData()
        self.tblHome.reloadData()
        
        
        self.pageController.progress = 0
      //  self.pageController.elementInactiveColor = UIColor(red: 248/255, green: 202/255, blue: 204/255, alpha: 1)
        //self.pageController.inactiveTransparency = 0.1
        self.pageController.currentPageTintColor = UIColor.white
        //self.pageController.tintColor = UIColor(red: 248/255, green: 202/255, blue: 204/255, alpha: 1)
        // UIColor(red: 248/255, green: 202/255, blue: 204/255, alpha: 1)
        
//        setUpNextPreviousButton()
//        startTimer()
        
    }
   
    /*func setUpNextPreviousButton()
    {
        if(arrHeaderImage.count == 0)
        {
            self.btnPreviousOutlet.isHidden = true
            self.btnPreviousOutlet.isHidden = true
     
        }
     
    }*/
    
    
}

//MARK:- Slider Imageview

extension ViewController
{
    @objc func scrollToNextCell()
    {
        var scrollxpos = collectionOfHeader.contentOffset.x
        
        var visibleRect = CGRect()
        
        if scrollxpos + collectionOfHeader.frame.size.width == collectionOfHeader.contentSize.width
        {
            selectedIndex = 0
            scrollxpos = 0
            visibleRect = CGRect(x: scrollxpos, y: collectionOfHeader.frame.origin.y, width: collectionOfHeader.frame.size.width, height: collectionOfHeader.frame.size.height)
            let pageNumber = round(scrollxpos / collectionOfHeader.frame.size.width)
            pageController.progress = Double(pageNumber)
        }
        else
        {
            selectedIndex += 1
            visibleRect = CGRect(x: (scrollxpos +  collectionOfHeader.frame.size.width), y: collectionOfHeader.frame.origin.y, width: collectionOfHeader.frame.size.width, height: collectionOfHeader.frame.size.height)
            let pageNumber = round((scrollxpos +  collectionOfHeader.frame.size.width)/collectionOfHeader.frame.size.width)
            pageController.progress = Double(pageNumber)
        }
        
        collectionOfHeader.scrollRectToVisible(visibleRect, animated: true)
        
    }
    
   
    func startTimer() {
        
        let timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(scrollToNextCell), userInfo: nil, repeats: true)
        
    }
}

//MARK:- Collectionview Delegate & Datasource

extension ViewController :UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(arrHeaderImage.count != 0)
        {
            return arrHeaderImage.count
        }
        return 1
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionCell", for: indexPath) as! HomeCollectionCell
        
        if(arrHeaderImage.count == 0)
        {
            cell.imgOfHeader.image = UIImage(named:"banner_home_page.png")
            return cell
        }
        
        let dict = arrHeaderImage[indexPath.row]
        cell.setData(dict)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
}

//MARK:- Tableview Delegate & Datasource

extension ViewController : UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrHome.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTablviewCell") as! HomeTablviewCell
        let dict = arrHome[indexPath.row]
        cell.setData(dict)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        objOrder.strSelectedStoreId = arrHome[indexPath.row]["id"].stringValue
        objOrder.strSelectStore = arrHome[indexPath.row]["store_name"].stringValue
        objOrder.strStorePhoneNumber = arrHome[indexPath.row]["store_phone"].stringValue
        objOrder.strStoreAddress = arrHome[indexPath.row]["store_address"].stringValue
        SaveOrderDetailsToDefaults()
        
        let arrayCart = GetCartData()
        if arrayCart.count != 0 {
            let arrFilter = arrayCart.filter({$0["store_id"].stringValue == objOrder.strSelectedStoreId})
            if arrFilter.count == 0 {
                displayAlertWithTitle(getCommonString(key: "Pizzaapp_key"), andMessage: "Your cart has an item with another store, It will remove if you add this item. Are you sure you want to add this item?", buttons: ["Yes","No"], completion: {(tag) in
                    if tag == 0 {
                        self.deleteAllItemsfromCart()
                        let obj = objStoryboard.instantiateViewController(withIdentifier: "DeliveryTypeVC") as! DeliveryTypeVC
                        obj.dict = self.arrHome[indexPath.row]
                        dictStoreDetail = self.arrHome[indexPath.row]
                        self.navigationController?.pushViewController(obj, animated: true)
                    }
                })
                
            } else {
                let obj = objStoryboard.instantiateViewController(withIdentifier: "DeliveryTypeVC") as! DeliveryTypeVC
                obj.dict = arrHome[indexPath.row]
                dictStoreDetail = arrHome[indexPath.row]
                self.navigationController?.pushViewController(obj, animated: true)
            }
        } else {
            let obj = objStoryboard.instantiateViewController(withIdentifier: "DeliveryTypeVC") as! DeliveryTypeVC
            obj.dict = arrHome[indexPath.row]
            dictStoreDetail = arrHome[indexPath.row]
            self.navigationController?.pushViewController(obj, animated: true)
        }
        
       
    }
}

//MARK:- Action Zone

extension ViewController
{
    @IBAction func btnNextAction(_ sender:UIButton)
    {
        self.btnPreviousOutlet.isHidden = false
        print("selected \(selectedIndex)")
        print("arrHeaderImage Count \(arrHeaderImage.count-1)")
        
        if selectedIndex < arrHeaderImage.count-1
        {
            selectedIndex += 1
            self.pageController.progress = Double(selectedIndex)
            self.collectionOfHeader.scrollToItem(at: IndexPath(item: selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
            
            return
        }
        self.btnNextOutlet.isHidden = true
       
    }
    
    @IBAction func btnPreviousAction(_ sender:UIButton)
    {
        self.btnNextOutlet.isHidden = false
        if selectedIndex == 0
        {
            self.btnPreviousOutlet.isHidden = true
            return
        }
        selectedIndex -= 1
        self.pageController.progress = Double(selectedIndex)
        self.collectionOfHeader.scrollToItem(at: IndexPath(item: selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
    }
}

//MARK:- Scrollview Delegate

extension ViewController:UIScrollViewDelegate
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == collectionOfHeader
        {
            let count = scrollView.contentOffset.x / scrollView.frame.size.width
            self.pageController.progress = Double(Int(count))
            selectedIndex = Int(count)
        }
    }
}

//MARK:- Service

extension ViewController
{
    func getStoreList()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kStoreList)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang
                
            ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrHome = []
                        self.arrHome = json["data"].arrayValue
                        self.vwContent.isHidden = false
                    }
                    else
                    {
                        self.arrHome = []
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.tblHome.reloadData()
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
    func getBannerImages()
    {
        if (Alamofire.NetworkReachabilityManager()?.isReachable)!
        {
            let url = "\(kBasicURL)\(kGetBannerList)"
            
            print("URL: \(url)")
            
            let param =  ["lang":strLang
                
            ]
            
            
            print("Param : \(param)")
            
            showLoader()
            
            CommonService().Service(url: url, param: param) { (respones) in
                
                self.stopLoader()
                
                if let json = respones.value
                {
                    print("JSON : \(json)")
                    
                    if json["flag"].stringValue == strSuccessResponse
                    {
                        self.arrHeaderImage = []
                        self.arrHeaderImage = json["data"].arrayValue
                        self.pageController.numberOfPages = self.arrHeaderImage.count
                        self.startTimer()
                    }
                    else
                    {
                        self.arrHeaderImage = []
                        makeToast(message: json["msg"].stringValue)
                    }
                    self.collectionOfHeader.reloadData()
                }
                else
                {
                    makeToast(message: getCommonString(key: "Something_went_wrong_key"))
                }
            }
            
        }
        else
        {
            makeToast(message: getCommonString(key: "No_internet_connection_key"))
        }
    }
}












