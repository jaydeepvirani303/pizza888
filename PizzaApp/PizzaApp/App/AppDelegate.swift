//
//  AppDelegate.swift
//  PizzaApp
//
//  Created by Jaydeep on 19/12/18.
//  Copyright © 2018 Jaydeep. All rights reserved.
//

import UIKit
import GoogleMaps
import RealmSwift
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate
{

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        GMSServices.provideAPIKey(kGoogleMapsKey)
        
//        PayPalMobile .initializeWithClientIds(forEnvironments: [PayPalEnvironmentProduction: kPaypalLiveClientId,PayPalEnvironmentSandbox: kPaypalSandboxClientId])
        
        PayPalMobile.initializeWithClientIds(forEnvironments: [PayPalEnvironmentSandbox: kPaypalSandboxClientId])
        
        Stripe.setDefaultPublishableKey(kStripeKey)
        
        let cachesDirectoryPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0]
        let cachesDirectoryURL = NSURL(fileURLWithPath: cachesDirectoryPath)
        let fileURL = cachesDirectoryURL.appendingPathComponent("Default.realm")
        
        let config = Realm.Configuration(fileURL: fileURL)
        do {
            globalRealm = try Realm(configuration: config)
        } catch {
            print("Error")
        }
        
        print("Realm config URL - ",config)
        
        let obj = objStoryboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        window?.rootViewController?.navigateUserWithLG(obj: obj)
      
        self.window?.makeKeyAndVisible()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

