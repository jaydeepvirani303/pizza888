//
//  CheckoutUser.swift
//  PizzaApp
//
//  Created by Khushbu on 27/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import RealmSwift
import SwiftyJSON

class CheckoutUser: Object
{
    @objc dynamic var id = 0
    @objc dynamic var strName  = ""
    @objc dynamic var strPhoneNumber  = ""
    @objc dynamic var strEmailAddress  = ""
    @objc dynamic var strUnitNumber  = ""
    @objc dynamic var strStreetNumber  = ""
    @objc dynamic var strStreetName  = ""
    @objc dynamic var strSuburbName  = ""
    @objc dynamic var strDeliveryInsturction  = ""
    @objc dynamic var strReceiveMail  = ""
    @objc dynamic var strReceiveSMS  = ""
    @objc dynamic var strRememberPickupDetil  = ""
    @objc dynamic var strSuburbId  = ""
    @objc dynamic var strSuburbCost  = "0.0"
    
    //MARK:- Set Primary key
    override class func primaryKey() -> String {
        return "id"
    }
    
    func setData(objUser : User,PKId:Int) -> CheckoutUser
    {
        
        let objUserCheckout = CheckoutUser()
        objUserCheckout.id = PKId
        objUserCheckout.strName = objUser.strName
        objUserCheckout.strPhoneNumber = objUser.strPhoneNumber
        objUserCheckout.strEmailAddress = objUser.strEmailAddress
        objUserCheckout.strUnitNumber = objUser.strUnitNumber
        objUserCheckout.strStreetNumber = objUser.strStreetNumber
        objUserCheckout.strStreetName = objUser.strStreetName
        objUserCheckout.strSuburbName = objUser.strSuburbName
        objUserCheckout.strDeliveryInsturction = objUser.strDeliveryInsturction
        objUserCheckout.strRememberPickupDetil = objUser.strRememberPickupDetil
        objUserCheckout.strSuburbId = objUser.strSuburbId
        objUserCheckout.strSuburbCost = objUser.strSuburbCost
        return objUserCheckout
    }
    func UpdateUserData(objUser : User,PKId:Int)
    {
        printUpdateData(objUser : objUser)
        
        let checkedUser = Array(globalRealm.objects(CheckoutUser.self)).filter { $0.id == PKId}
        if checkedUser.count != 0
        {
            let objFirst  = checkedUser.first
            try? globalRealm.write
            {
                objFirst?.strName = objUser.strName
                objFirst?.strPhoneNumber = objUser.strPhoneNumber
                objFirst?.strEmailAddress = objUser.strEmailAddress
                objFirst?.strUnitNumber = objUser.strUnitNumber
                objFirst?.strStreetNumber = objUser.strStreetNumber
                objFirst?.strStreetName = objUser.strStreetName
                objFirst?.strSuburbName = objUser.strSuburbName
                objFirst?.strSuburbId = objUser.strSuburbId
                objFirst?.strSuburbCost = objUser.strSuburbCost
                objFirst?.strDeliveryInsturction = objUser.strDeliveryInsturction
                objFirst?.strRememberPickupDetil = objUser.strRememberPickupDetil
                objFirst?.strReceiveSMS = objUser.strReceiveSMS
                objFirst?.strReceiveMail = objUser.strReceiveMail
                objFirst?.strRememberPickupDetil = objUser.strRememberPickupDetil
                globalRealm.add(objFirst!, update: .all)
//                globalRealm.add(objFirst!, update: true)
            }
        }
    }
    func printUpdateData(objUser : User)
    {
        print("updated objUser - ",objUser)

        print("strName - ",objUser.strName)
        print("strPhoneNumber - ",objUser.strPhoneNumber)
        print("strEmailAddress - ",objUser.strEmailAddress)
        print("strUnitNumber - ",objUser.strUnitNumber)
        print("strStreetNumber - ",objUser.strStreetNumber)
        print("strStreetName - ",objUser.strStreetName)
        print("strSuburbName - ",objUser.strSuburbName)
        print("strDeliveryInsturction - ",objUser.strDeliveryInsturction)
        print("strRememberPickupDetil - ",objUser.strRememberPickupDetil)
        print("strSuburbId - ",objUser.strSuburbId)
        print("strSuburbCost - ",objUser.strSuburbCost)
        
    }
}
