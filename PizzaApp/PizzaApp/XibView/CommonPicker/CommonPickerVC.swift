//
//  CommonPickerVC.swift
//  Muber
//
//  Created by Jaydeep on 26/03/18.
//

import UIKit
import Alamofire
import AlamofireSwiftyJSON
import SwiftyJSON

enum selectDropdown
{
    case addcard
    case storeTime
    case suburb
}

protocol CommonPickerDelegate
{
    func setValuePicker(selectedDict:JSON)
}


class CommonPickerVC: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet var pickerVw: UIPickerView!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var vwBtns: UIView!
    
    //MARK: - Variable
    
    var strValueStore = String()
    var arrayPicker: [JSON] = []
    var pickerDelegate : CommonPickerDelegate?
    var selectedYear = String()
    var selectedMonth = String()
    var isComeFromExpiryMonth = Bool()
    var strType = ""
    var selectedDropdown = selectDropdown.addcard
    
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Array : \(self.arrayPicker)")
        vwBtns.backgroundColor = UIColor.appThemeRedColor
        
        btnDone.setTitle(getCommonString(key: "Done_key").uppercased(), for: .normal)
        btnDone.titleLabel?.font = themeFont(size: 20, fontname: .semibold)
        btnDone.setTitleColor(.white, for: .normal)
        
        btnCancel.setTitle(getCommonString(key: "Cancel_key").uppercased(), for: .normal)
        btnCancel.titleLabel?.font = themeFont(size: 20, fontname: .semibold)
        btnCancel.setTitleColor(.white, for: .normal)
        
    }
    
}


//MARK: - IBAction
extension CommonPickerVC
{    
    @IBAction func btnCancelTapped(sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func btnDoneTapped(sender: UIButton)
    {        
        let dict = arrayPicker[self.pickerVw.selectedRow(inComponent: 0)]
        self.dismiss(animated: true, completion: nil)
        pickerDelegate?.setValuePicker(selectedDict: dict)
        
    }
    
}

extension CommonPickerVC: UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayPicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        switch selectedDropdown {
            
        case .addcard:
            return arrayPicker[row]["value"].stringValue
            
        case .storeTime:
            return arrayPicker[row]["time"].stringValue
    
        case .suburb:
            return "\(arrayPicker[row]["location_name"].stringValue) (\(arrayPicker[row]["delivery-cost"].stringValue))"

        }
    }
}






