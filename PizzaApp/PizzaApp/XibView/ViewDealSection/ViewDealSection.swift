//
//  ViewDealSection.swift
//  PizzaApp
//
//  Created by Jaydeep on 05/02/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class ViewDealSection: UIView {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblItmeName: UILabel!
    @IBOutlet weak var btnExpandOutlet: UIButton!
    @IBOutlet weak var btnExpandOutetOutlet: UIButton!
    
    
    //MARK:- ViewLife Cycle

    override func awakeFromNib() {
        [lblItmeName].forEach { (lbl) in
            lbl?.textColor = UIColor.appThemeBlackColor
            lbl?.font = themeFont(size: 18, fontname: .semibold)
        }
        
    }

}
